<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('issues')->group(function() {
    Route::get('/admin', 'IssueAdminController@index')->name('issues.admin.index')->middleware('permission:issue-admin');
    Route::get('/admin/{id}', 'IssueAdminController@show', function($id) {
        return $id;
    })->name('issues.admin.show')->middleware('permission:issue-admin');

    Route::get('/', 'IssueController@index')->name('issues.index');
    Route::get('/create', 'IssueController@create')->name('issues.create');
    Route::get('/{issue}', 'IssueController@show', function ($issue) {
        return $issue;
    })->name('issues.show');

    Route::post('/', 'IssueController@store')->name('issues.store');

    Route::get('/categories/admin', 'IssueCategoryAdminController@index')->name('issues.admin.categories.index');
});
