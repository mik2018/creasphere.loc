@extends('layouts.app')

@push('stylesheets')
    <link href="{{ asset('css/custom/issues_admin.css') }}" rel="stylesheet">
@endpush
@section('content')
    <h1>Категори обращений пользоваелей</h1>

    <p>
        <a href="{{ route('issues.admin.categories.index') }}">Категории обращений</a>
    </p>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Название</th>
            <th scope="col">Родительская категория</th>
        </tr>
        </thead>
        <tbody>
        @foreach($categories as $category)
            <tr>
                <th scope="row">{{ $category->id }}</th>
                <td>{{ $category->title }}</td>
                <td>{{ $category->parent->title ?? '' }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop
