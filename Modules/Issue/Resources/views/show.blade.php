@extends('layouts.app')

@push('stylesheets')
    <link href="{{ asset('css/custom/issues_admin.css') }}" rel="stylesheet">
@endpush
@section('content')
    <h1>Обращение {{ $issue->title }}</h1>

    <h2>Тип обращения: {{ $issue->category->title }}</h2>
    <div>
        {{ $issue->content }}
    </div>
@stop
