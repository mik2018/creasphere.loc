<div class="form-group">
    {{ Form::label('title', 'Название обращения') }}
    {{ Form::text('title', null, ['class' => 'form-control', 'required']) }}
</div>
<div class="form-group">
    {{ Form::label('category_id', 'Тип обращения') }}
    <select class="form-control" name="category_id" id="category_id" required="">
        @foreach($categories as $category)
            <option value="{{ $category->id }}">{{ $category->title }}</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    {{ Form::label('content', 'Описание проблемы') }}
    {{ Form::textarea('content', null, ['class' => 'form-control', 'required']) }}
</div>