@extends('layouts.app')

@push('stylesheets')
    <link href="{{ asset('css/custom/issues_admin.css') }}" rel="stylesheet">
@endpush
@section('content')
    <h1>Сформулируйте ваш запрос</h1>

    {!! Form::open(['route' => ['issues.store']]) !!}
    @include('issue::_form')
    <button class="btn btn-primary" role="submit">Отправить обращение</button>
    {!! Form::close() !!}
@stop
