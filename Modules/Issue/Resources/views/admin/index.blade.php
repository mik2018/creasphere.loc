@extends('layouts.app')

@push('stylesheets')
    <link href="{{ asset('css/custom/issues_admin.css') }}" rel="stylesheet">
@endpush
@section('content')
    <h1>Обращения пользователей</h1>

    <p>
        <a href="{{ route('issues.admin.categories.index') }}">Категории обращений</a>
    </p>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Название</th>
            <th scope="col">Категория</th>
            <th scope="col">Пользователь</th>
        </tr>
        </thead>
        <tbody>
            @forelse($issues as $issue)
                <tr>
                    <th scope="row">{{ $issue->id }}</th>
                    <td>
                        <a href="{{ route('issues.admin.show', $issue) }}">{{ $issue->title }}</a>
                    </td>
                    <td>{{ $issue->category->title }}</td>
                    <td>{{ $issue->user->name }} {{ $issue->user->surname }}</td>
                </tr>
            @empty
                <p>Нет обращений</p>
            @endforelse
        </tbody>
        {{ $issues->links() }}
    </table>
@stop
