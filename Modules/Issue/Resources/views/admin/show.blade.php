@extends('layouts.app')

@push('stylesheets')
    <link href="{{ asset('css/custom/issues_admin.css') }}" rel="stylesheet">
@endpush
@section('content')
    <h1>#{{ $issue->id }}Обращения пользователя {{ $issue->user->name }} {{ $issue->user->surname }}</h1>

    <h2>Заголовок: {{ $issue->title }}</h2>
    <h2>Тема: {{ $issue->category->title ?? '' }}</h2>

    <div>
        {{ $issue->content }}
    </div>
@stop
