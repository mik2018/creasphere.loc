<?php

namespace Modules\Issue\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Modules\Issue\Entities\IssueCategory;

class Issue extends Model
{
    protected $table = 'issues';
    protected $fillable = ['user_id', 'admin_id', 'category_id', 'title', 'content', 'updated_at'];

    public function category()
    {
        return $this->hasOne(IssueCategory::class, 'id', 'category_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id');
    }

    public function admin()
    {
        return $this->hasOne(User::class, 'id');
    }
}
