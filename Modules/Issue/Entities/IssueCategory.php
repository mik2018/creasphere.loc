<?php

namespace Modules\Issue\Entities;

use Illuminate\Database\Eloquent\Model;

class IssueCategory extends Model
{
    protected $table = 'issues_categories';
    protected $fillable = ['title', 'parent_id'];

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }
}
