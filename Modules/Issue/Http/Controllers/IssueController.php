<?php

namespace Modules\Issue\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Issue\Entities\Issue;
use Modules\Issue\Entities\IssueCategory;

class IssueController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('issue::index', [
            'issues' => Issue::where('user_id', auth()->user()->id)->paginate(25)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('issue::create', [
            'categories' => IssueCategory::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = array_merge($request->except(['_token']), ['user_id' => auth()->user()->id]);
        Issue::create($data);

        return redirect()->route('issues.index');
    }

    /**
     * Show the specified resource.
     * @param Issue $issue
     * @return Response
     */
    public function show(Issue $issue)
    {
        return view('issue::show', [
            'issue' => $issue
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('issue::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
