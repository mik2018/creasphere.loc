<?php

namespace Modules\Issue\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class IssuesCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('issues_categories')->insert([
            [
                'parent_id' => 'null',
                'title' => 'Техническая неисправность'
            ],
            [
                'parent_id' => 'null',
                'title' => 'Предложения по улучшению сайта'
            ],
            [
                'parent_id' => 'null',
                'title' => 'Жалоба на пользователя/контент'
            ],
        ]);
    }
}
