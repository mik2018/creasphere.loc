@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-9 col-md-push-3">
            <div class="be-large-post">
                <div class="info-block">
                    <div class="be-large-post-align">
                        {{--<span><i class="fa fa-thumbs-o-up"></i> 253</span>--}}
                        {{--<span><i class="fa fa-eye"></i> 753</span>--}}
                        {{--<span><i class="fa fa-comment-o"></i> 50</span>--}}
                    </div>
                </div>
                <div class="blog-content be-large-post-align">
                    <h5 class="be-post-title">
                        {{ $resume->title }}
                        @if(authAndAccess('resume-edit'))
                            <a href="{{ route('my_resume.edit', $resume) }}" class="size-2 btn be-follow-type"><i class="fa fa-pencil"></i>Редактировать резюме</a>
                        @endif
                    </h5>

                    <span class="be-text-tags">
                        Проф. область: {{ $resume->profCategory->title }}
                    </span>
                    <p>
                        {{ $resume->description }}
                    </p>
                    <hr>
                    <div class="clear"></div>
                    {{--<div class="be-largepost-iframe--}}
                    {{--embed-responsive embed-responsive-16by9">--}}
                        {{--<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/124626586" frameborder="0"></iframe>--}}
                    {{--</div>--}}

                    <h3>Опыт работы</h3><br>
                    @forelse($resume->experiences as $job)
                        <div class="be-comment">
                            <div class="be-img-comment">
                                <a href="blog-detail-2.html">
                                    <img src="img/c1.png" alt="" class="be-ava-comment">
                                </a>
                            </div>
                            <div class="be-comment-content">
                                <span class="be-comment-name">
                                    <h3>Компания: {{ $job->company }}</h3>
                                </span>
                                <span class="be-comment-time">
                                    <i class="fa fa-clock-o"></i>
                                    Дата поступления на работу: {{ \Carbon\Carbon::parse($job->first_day)->format('d-M-y') }}
                                </span>
                                <p class="be-comment-text">
                                    Должность: {{ $job->position }} <br>
                                </p>
                                <p class="be-comment-text">
                                    Рабочие обязанности:<br> {{ $job->job_tasks }}
                                </p>
                                <p class="be-comment-text">
                                    Достижения:<br> {{ $job->achievements }}
                                </p>
                                <span class="be-comment-name">

                                </span>
                                @if($job->last_day)
                                <span class="be-comment-time">
                                    <i class="fa fa-clock-o"></i>
                                    Дата увольнения: {{ \Carbon\Carbon::parse($job->last_day)->format('d-M-y') }}
                                </span>
                                @else
                                    <span class="be-comment-time">
                                    <i class="fa fa-clock-o"></i>
                                    Сейчас работаю здесь
                                </span>
                                @endif
                                <hr>
                            </div>
                        </div>
                    @empty
                        <p>Опыт не указан</p>
                    @endforelse

                    <h3>Образование</h3><br>
                    @forelse($resume->educations as $education)
                        <div class="be-comment">
                            {{--<div class="be-img-comment">--}}
                                {{--<a href="blog-detail-2.html">--}}
                                    {{--<img src="img/c1.png" alt="" class="be-ava-comment">--}}
                                {{--</a>--}}
                            {{--</div>--}}
                            <div class="be-comment-content">
                                <span class="be-comment-name">
                                    <h3>Учреждение образования: {{ $education->institution }} ({{ $education->level->name }})</h3>
                                </span>
                                {{--<span class="be-comment-time">--}}
                                    {{--<i class="fa fa-clock-o"></i>--}}
                                    {{--Дата поступления на работу: {{ \Carbon\Carbon::parse($job->first_day)->format('d-M-y') }}--}}
                                {{--</span>--}}
                                @if($education->faculty)
                                <p class="be-comment-text">
                                    Факультет: {{ $education->faculty }}
                                </p>
                                @endif
                                <p class="be-comment-text">
                                    Специальность: {{ $education->specialty }}
                                </p>
                                <span class="be-comment-name">

                                </span>
                                <span class="be-comment-time">
                                    <i class="fa fa-clock-o"></i>
                                    Дата окончания: {{ \Carbon\Carbon::parse($education->last_day)->format('d-M-y') }}
                                </span>
                                <hr>
                            </div>
                        </div>
                    @empty
                        <p>Образование не указано</p>
                    @endforelse

                    <div class="post-text">
                        <h3>Несколько слов о себе</h3>
                        {{ $user->about }}
                    </div>
                </div>
                <div class="be-large-post-align">
                    <h3 class="letf-menu-article">
                        Интересы
                    </h3>
                    <div class="tags_block clearfix">
                        <ul>
                            @forelse($user->tags as $tag)
                                <li><a >{{ $tag->name }}</a></li>
                            @empty
                                интересы не указаны
                            @endforelse
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-md-pull-9 left-feild">
            <div class="be-user-block">
                <div class="be-user-detail">
                    <a class="be-ava-user" href="#">
                        <img class="be-ava-img" src="{{ asset('img/resumes/' . $resume->getPathFromHash($resume->image)) }}" alt="">
                    </a>
                    <p class="be-use-name">{{ $user->name . ' ' . $user->surname }}</p>
                    <p class="be-user-info">
                        <a href="mailto:{{$resume->email}}">E-mail: {{ $resume->email }}</a>
                    </p>
                    <p class="be-user-info">Телефон: {{ $resume->phone }}</p>
                    <p class="be-user-info">Дата регистрации: {{ \Carbon\Carbon::parse($user->birthday)->format('d-M-y') }}</p>

                    <p class="be-user-info">Место работы: {{ $user->job }}</p>
                    <p class="be-user-info">Профессия: {{ $user->profession ?? '- / -'}}</p>
                    <p><span><i class="fa fa-eye"></i> {{ $resume->getAndUpdateViews() }}</span></p>
                </div>
                {{--<div class="be-user-activity-block">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-lg-12">--}}
                            {{--<a href="{{ route('my_profile.edit') }}" class="be-user-activity-button be-follow-type"><i class="fa fa-pencil"></i>Редактировать профиль</a>--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-12">--}}
                            {{--<a href="blog-detail-2.html" class="be-user-activity-button be-follow-type"><i class="fa fa-plus"></i>Подписаться на новости</a>--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-12">--}}
                            {{--<a href="blog-detail-2.html" class="col-lg-6 be-user-activity-button be-message-type"><i class="fa fa-envelope-o"></i>Отправить сообщение</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
            {{--<a href="blog-detail-2.html" class="be-button-vidget blue-style"><i class="fa fa-thumbs-o-up"></i>LIKE PROJECT</a>--}}
            {{--<a href="blog-detail-2.html" class="be-button-vidget grey-style"><i class="fa fa-file-o"></i>ADD TO COLLECTION</a>--}}
        </div>
    </div>
@stop
