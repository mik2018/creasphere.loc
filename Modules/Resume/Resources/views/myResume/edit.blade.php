@extends('layouts.app')


@section('content')
    <div class="card">
        <div class="card-header">
            <h1>{{ $resume->title }}</h1>
            <p><a href="{{ route('my_resume.create') }}">Создать новое резюме</a></p>
        </div>
        <div class="card-body">
            {{ Form::model($resume, ['route' => ['my_resume.update', $resume], 'method' => 'put', 'files' => true]) }}
                <div class="card">
                    <div class="card-header">
                        <h2>Основные данные</h2>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            {{ Form::label('title', 'Дайте название Bашему резюме') }}
                            {{ Form::text('title', $resume->title ?? '', ['class' => 'form-control', 'required' => '']) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('image', 'Картинка', ['class' => 'control-label', 'multiple']) }}
                            {{ Form::file('image', null, ['class' => 'form-control']) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('parent_id', 'Профессия') }}
                            <select name="resume_prof_categories_id" id="resume_prof_categories_id" class="parent_id form-control" required>
                                <option value="0" disabled selected>Не выбрана</option>
                                @include('resume::myResume.partials.editcategories')
                            </select>
                        </div>

                        <div class="form-group">
                            {{ Form::label('description', 'Опишите Вашу деятельность') }}
                            {{ Form::textarea('description', $resume->description ?? '', ['class' => 'form-control', 'rows' => 3]) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('phone', 'Ваш телефон') }}
                            {{ Form::text('phone', $resume->phone ?? '', ['class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('email', 'Ваш E-mail') }}
                            {{ Form::text('email', $resume->email ?? '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>
                {{--{{ dd(json_encode($resume->experiences)) }}--}}
                <h2>Опыт работы</h2>
                <resume-form-experience-component :educations_list="@json($resume->educations)"></resume-form-experience-component>

                <h2>Образование</h2>
                <resume-form-education-component></resume-form-education-component>
            </div>

            <button class="m-4 btn btn-primary"><i class="fa fa-check-square-o" aria-hidden="true"></i> Сохранить изменения</button>
            {!! Form::close() !!}
        </div>
    </div>
@stop
