<div class="card">
    <div class="card-header">
        <h2>Основные данные</h2>
    </div>
    <div class="card-body">
        <div class="form-group">
            {{ Form::label('title', 'Дайте название Bашему резюме') }}
            {{ Form::text('title', $resume->title ?? '', ['class' => 'form-control', 'required' => '']) }}
        </div>

        <div class="form-group">
            {{ Form::label('parent_id', 'Профессия') }}
            <select name="resume_prof_categories_id" id="resume_prof_categories_id" class="parent_id form-control" required>
                <option value="0" disabled selected>Не выбрана</option>
                @include('resume::myResume.partials.categories')
            </select>
        </div>

        <div class="form-group">
            {{ Form::label('image', 'Загрузите картинку', ['class' => 'control-label', 'multiple']) }}
            {{ Form::file('image', null, ['class' => 'form-control']) }}
        </div>

        <div class="form-group">
            {{ Form::label('description', 'Опишите Вашу деятельность') }}
            {{ Form::textarea('description', $resume->description ?? '', ['class' => 'form-control', 'rows' => 3]) }}
        </div>
        <div class="form-group">
            {{ Form::label('phone', 'Ваш телефон') }}
            {{ Form::text('phone', $resume->phone ?? '', ['class' => 'form-control']) }}
        </div>
        <div class="form-group">
            {{ Form::label('email', 'Ваш E-mail') }}
            {{ Form::text('email', $resume->email ?? '', ['class' => 'form-control']) }}
        </div>
    </div>
</div>
<br>
<br>

<h2>Опыт работы</h2>
<resume-form-experience-component></resume-form-experience-component>

<h2>Образование</h2>
<resume-form-education-component></resume-form-education-component>

<button type="submit" class="btn btn-primary">Добавить резюме</button>
