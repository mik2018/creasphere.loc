@extends('layouts.app')

@section('content')
<!--todo потом доделать всю форму-->
<!--todo сделать подгрузку файлов(аватарки резюме) через форму-->
<!--todo потом доработать метод в контроллере-->
<!--todo потом переделать метод edit и update в контроллере-->

<!--todo поправить хедер: убрать лишние ссылки-->
<div class="card">
    <div class="card-header">
        <h1>{{ $pagename }}</h1>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="card-body">
        {!! Form::open(['route' => 'my_resume.store', 'files' => true]) !!}
            @include('resume::myResume._form')
        {!! Form::close() !!}
    </div>
</div>

    {{--SCRYPT FOR DYNAMICAL HTML-FORM(add a new rows online)--}}
    {{--<script>--}}
        {{--jq2 = jQuery.noConflict();--}}
        {{--jq2(function( $ ) {--}}
            {{--// Код, использующий $ "идет" сюда, а фактическим объектом jQuery является jq2--}}

            {{--$(".add").click(function() {--}}
                {{--$("form > .card > .card-body > .form-group > span:first-child").clone(true).insertBefore("form > .card > .card-body > .form-group > span:last-child");--}}
                {{--return false;--}}
            {{--});--}}

            {{--$(".remove").click(function() {--}}
                {{--$(this).parent().remove();--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}
@stop
