@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h1>Мои резюме</h1>
            <p><a href="{{ route('my_resume.create') }}">Создать новое резюме</a></p>
        </div>
        <div class="card-body">
            @forelse($resumes as $resume)
                <p class="d-inline">
                    <a href="{{ route('my_resume.show', $resume) }}">{{ $resume->title }}</a> 
                    <a class="d-inline" href="{{ route('my_resume.edit', $resume) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
                    <form method="post" class="d-inline" onsubmit="if(!confirm('Вы действительно хотите удалить резюме?')){return false;}" action="{{ route('my_resume.destroy', $resume) }}">
                        @csrf
                        @method('delete')
                        <button class="btn btn-link" role="submit"><i class="fa fa-times" aria-hidden="true"></i></button>
                    </form>
                </p>
            @empty
                <p>Нет резюме</p>
            @endforelse
        </div>
    </div>
@stop
