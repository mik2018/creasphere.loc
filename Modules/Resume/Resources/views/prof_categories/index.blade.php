@extends('layouts.app')

@section('content')
    <h1>Проф. категории для резюме</h1>
    <a href="{{ route('prof_categories.create') }}">Добавить категорию</a>

    @include('resume::prof_categories.partials.categorytree')
@stop
