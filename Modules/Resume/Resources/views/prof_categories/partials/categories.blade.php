@foreach($categories as $prof_categoryItem)
    <option value="{{ $prof_categoryItem->id ?? ''}}"
            @if(isset($prof_category->parent_id) and $prof_category->parent_id == $prof_categoryItem->id)
            selected=""
            @endif

            @if(isset($prof_category->id) and $prof_category->id == $prof_categoryItem->id)
            disabled=""
            @endif
    >
        {{ $delimiter ?? '' }} {{ $prof_categoryItem->title ?? '' }}
    </option>

    @isset($prof_categoryItem->children)
        <?php $variables = [
            'categories' => $prof_categoryItem->children,
            'delimiter' => ' - ' . $delimiter
        ];
        if (isset($prof_category)) {$variables[] = $prof_category;}
        ?>
        @include('resume::prof_categories.partials.categories', $variables)
    @endisset
@endforeach