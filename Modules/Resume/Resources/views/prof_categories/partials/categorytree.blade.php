<ul>
    @foreach($categories as $category)
        <li>
            {{ $category->title }}
            <a href="{{ route('prof_categories.edit', $category) }}"><i class="fa fa-pencil-square" aria-hidden="true"></i></a>


            <form class="d-inline" method="post" action="{{ route('prof_categories.destroy',$category) }}" onsubmit="if(!confirm('Удалить категорию?')){return false}">
                @csrf
                @method('delete')
                <button role="submit" class="btn btn-link d-inline">
                    <i class="fa fa-times-rectangle" aria-hidden="true"></i>
                </button>
            </form>
            @isset($category->children)
                @include('resume::prof_categories.partials.categorytree', [
                    'categories' => $category->children,
                    'delimiter' => ' - ' . $delimiter
                ])
            @endisset
        </li>
    @endforeach
</ul>