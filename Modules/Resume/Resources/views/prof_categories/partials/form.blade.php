<div class="form-group">
    {{ Form::label('title', 'Название категории') }}
    {{ Form::text('title', $prof_category->title ?? '', ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('parent_id', 'Текст комментария') }}
    <select name="parent_id" id="parent_id" class="parent_id form-control">
        <option value="0"> -- Без родительской категории</option>
        @include('resume::prof_categories.partials.categories')
    </select>
</div>
<button role="submit" class="btn btn-primary">{{ $buttonText }}</button>