@extends('layouts.app')

@section('content')
    <h1>Проф. категории для резюме</h1>
    {!! Form::open(['route' => 'prof_categories.store']) !!}
        @include('resume::prof_categories.partials.form')
    {!! Form::close() !!}
@stop
