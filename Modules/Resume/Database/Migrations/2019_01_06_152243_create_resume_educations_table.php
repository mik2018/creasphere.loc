<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResumeEducationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resume_educations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('resume_id')->unsigned();
            $table->foreign('resume_id')
                ->references('id')
                ->on('resumes')
                ->onDelete('cascade');

            $table->integer('level_id')->unsigned();
            $table->foreign('level_id')
                ->references('id')
                ->on('resume_education_levels');

            $table->string('institution');
            $table->string('faculty')->nullable();
            $table->string('specialty')->nullable();
            $table->timestamp('last_year');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resume_educations');
    }
}
