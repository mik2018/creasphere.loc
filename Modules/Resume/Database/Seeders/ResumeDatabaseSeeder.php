<?php

namespace Modules\Resume\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ResumeDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(ProfCategoryTableSeeder::class);
        $this->call(ResumeTableSeeder::class);
        $this->call(ResumeExperienceTableSeeder::class);
        $this->call(ResumeEducationLevelsTableSeeder::class);
    }
}
