<?php

namespace Modules\Resume\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('resume_prof_categories')->insert([
            [
                "id" => 1,
                "parent_id" => 2,
                "title" => "Дизайнер",
                "created_at" => "2018-11-30 06:10:29",
                "updated_at" => "2018-12-08 12:19:31",
            ],
            [
                "id" => 2,
                "parent_id" => 0,
                "title" => "Визуальные",
                "created_at" => "2018-12-02 14:27:15",
                "updated_at" => "2018-12-02 14:27:31",
            ],
            [
                "id" => 3,
                "parent_id" => 0,
                "title" => "Музыка",
                "created_at" => "2018-12-02 14:28:08",
                "updated_at" => "2018-12-08 12:18:07",
            ],
            [
                "id" => 4,
                "parent_id" => 3,
                "title" => "Композитор",
                "created_at" => "2018-12-02 14:28:37",
                "updated_at" => "2018-12-08 12:20:16",
            ],
            [
                "id" => 5,
                "parent_id" => 1,
                "title" => "Ландшафтный дизайнер",
                "created_at" => "2018-12-02 14:29:29",
                "updated_at" => "2018-12-08 12:19:37",
            ],
            [
                "id" => 6,
                "parent_id" => 1,
                "title" => "Дизайнер интерьера",
                "created_at" => "2018-12-02 14:29:32",
                "updated_at" => "2018-12-08 12:19:43",
            ],
            [
                "id" => 7,
                "parent_id" => 2,
                "title" => "Фотограф",
                "created_at" => "2018-12-08 12:17:37",
                "updated_at" => "2018-12-08 12:17:37",
            ],
            [
                "id" => 8,
                "parent_id" => 2,
                "title" => "Визажист",
                "created_at" => "2018-12-08 12:17:49",
                "updated_at" => "2018-12-08 12:17:49",
            ],
            [
                "id" => 9,
                "parent_id" => 3,
                "title" => "Певец",
                "created_at" => "2018-12-08 12:18:32",
                "updated_at" => "2018-12-08 12:18:32",
            ],
            [
                "id" => 10,
                "parent_id" => 3,
                "title" => "Аранжировщик",
                "created_at" => "2018-12-08 12:18:48",
                "updated_at" => "2018-12-08 12:18:56",
            ],
            [
                "id" => 11,
                "parent_id" => 1,
                "title" => "WEB-дизайнер",
                "created_at" => "2018-12-08 12:19:56",
                "updated_at" => "2018-12-08 12:19:56",
            ],
            [
                "id" => 12,
                "parent_id" => 2,
                "title" => "Архитектор",
                "created_at" => "2018-12-08 12:20:08",
                "updated_at" => "2018-12-08 12:20:08",
            ],
            [
                "id" => 13,
                "parent_id" => 0,
                "title" => "Танец",
                "created_at" => "2018-12-08 12:20:26",
                "updated_at" => "2018-12-08 12:20:26",
            ],
            [
                "id" => 14,
                "parent_id" => 13,
                "title" => "Хореограф",
                "created_at" => "2018-12-08 12:20:35",
                "updated_at" => "2018-12-08 12:20:35",
            ],
            [
                "id" => 15,
                "parent_id" => 13,
                "title" => "Танцор",
                "created_at" => "2018-12-08 12:20:41",
                "updated_at" => "2018-12-08 12:20:41",
            ],
            [
                "id" => 16,
                "parent_id" => 0,
                "title" => "Кино, театр",
                "created_at" => "2018-12-08 12:21:02",
                "updated_at" => "2018-12-08 12:21:02",
            ],
            [
                "id" => 17,
                "parent_id" => 16,
                "title" => "Сценарист",
                "created_at" => "2018-12-08 12:21:14",
                "updated_at" => "2018-12-08 12:21:14",
            ],
            [
                "id" => 18,
                "parent_id" => 16,
                "title" => "Режиссер",
                "created_at" => "2018-12-08 12:21:20",
                "updated_at" => "2018-12-08 12:21:20",
            ],
            [
                "id" => 19,
                "parent_id" => 16,
                "title" => "Видеооператор",
                "created_at" => "2018-12-08 12:21:28",
                "updated_at" => "2018-12-08 12:21:28",
            ],
            [
                "id" => 20,
                "parent_id" => 16,
                "title" => "Ведущий",
                "created_at" => "2018-12-08 12:21:43",
                "updated_at" => "2018-12-08 12:21:43",
            ],
            [
                "id" => 21,
                "parent_id" => 16,
                "title" => "Актёр",
                "created_at" => "2018-12-08 12:21:52",
                "updated_at" => "2018-12-08 12:21:52",
            ],
            [
                "id" => 22,
                "parent_id" => 0,
                "title" => "Слово",
                "created_at" => "2018-12-08 12:21:56",
                "updated_at" => "2018-12-08 12:21:56",
            ],
            [
                "id" => 23,
                "parent_id" => 22,
                "title" => "Писатель",
                "created_at" => "2018-12-08 12:22:13",
                "updated_at" => "2018-12-08 12:22:13",
            ],
            [
                "id" => 24,
                "parent_id" => 22,
                "title" => "Поэт",
                "created_at" => "2018-12-08 12:22:22",
                "updated_at" => "2018-12-08 12:22:22",
            ],
            [
                "id" => 25,
                "parent_id" => 22,
                "title" => "Копирайтер",
                "created_at" => "2018-12-08 12:22:34",
                "updated_at" => "2018-12-08 12:22:34",
            ],
            [
                "id" => 26,
                "parent_id" => 0,
                "title" => "Другое",
                "created_at" => "2018-12-08 12:22:53",
                "updated_at" => "2018-12-08 12:22:53",
            ],
            [
                "id" => 27,
                "parent_id" => 2,
                "title" => "Художник",
                "created_at" => "2018-12-08 12:23:39",
                "updated_at" => "2018-12-08 12:23:39",
            ],
            [
                "id" => 28,
                "parent_id" => 1,
                "title" => "Дизайнер одежды",
                "created_at" => "2018-12-08 12:23:58",
                "updated_at" => "2018-12-08 12:23:58",
            ],
            [
                "id" => 29,
                "parent_id" => 0,
                "title" => "Рукоделие",
                "created_at" => "2018-12-19 10:37:07",
                "updated_at" => "2018-12-19 10:37:07",
            ],
        ]);
    }
}
