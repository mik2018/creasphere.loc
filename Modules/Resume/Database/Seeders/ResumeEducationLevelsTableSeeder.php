<?php

namespace Modules\Resume\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ResumeEducationLevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('resume_education_levels')->insert([
            [
                "id" => 1,
                "parent_id" => 0,
                "name" => "Высшее",
            ],
            [
                "id" => 2,
                "parent_id" => 1,
                "name" => "Бакалавр",
            ],
            [
                "id" => 3,
                "parent_id" => 1,
                "name" => "Магистр",
            ],
            [
                "id" => 4,
                "parent_id" => 1,
                "name" => "Кандидат наук",
            ],
            [
                "id" => 5,
                "parent_id" => 1,
                "name" => "Доктор наук",
            ],
            [
                "id" => 6,
                "parent_id" => 0,
                "name" => "Среднее специальное",
            ],
            [
                "id" => 7,
                "parent_id" => 0,
                "name" => "Среднее",
            ],
        ]);
    }
}
