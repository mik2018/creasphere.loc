<?php

namespace Modules\Resume\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ResumeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('resumes')->insert([
            [
                "id" => 1,
                "user_id" => 1,
                "resume_prof_categories_id" => 4,
                "title" => "Nazvanie",
                "description" => "blaBlaBla",
                "email" => "email@email.com",
                "phone" => "+375 (44) 594-65-52",
            ],
            [
                "id" => 2,
                "user_id" => 1,
                "resume_prof_categories_id" => 4,
                "title" => "Resume 2",
                "description" => "Resume description",
                "email" => "email@email.com",
                "phone" => "+375 (44) 594-65-52",
            ]
        ]);
    }
}
