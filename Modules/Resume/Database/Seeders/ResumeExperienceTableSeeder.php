<?php

namespace Modules\Resume\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ResumeExperienceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('resume_experiences')->insert([
            [
                "id" => 1,
                "resume_id" => 1,
                "company" => "CreaSphere",
                "first_day" => "2018-11-01 00:00:00",
                "last_day" => "2018-12-06 00:00:00",
                "position" => "Cofounder",
                "job_tasks" => "job tasks",
                "achievements" => "job achievements",
                "created_at" => "2018-12-06 20:37:39",
                "updated_at" => "2018-12-06 20:38:14",
            ]
        ]);
    }
}
