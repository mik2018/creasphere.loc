<?php

namespace Modules\Resume\Entities;

use App\Traits\ImageTrait;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Modules\Profile\Entities\Tag;
use Modules\Statistics\Entities\Views;
use Modules\Support\Entities\Block;

class Resume extends Model
{
    use ImageTrait;

    protected $fillable = ['title', 'description', 'image', 'email', 'phone', 'user_id', 'resume_prof_categories_id'];

    public function experiences()
    {
        return $this->hasMany(ResumeExperience::class, 'resume_id');
    }

    public function educations()
    {
        return $this->hasMany(ResumeEducation::class, 'resume_id');
    }

    public function profCategory()
    {
        return $this->belongsTo(ProfCategory::class, 'resume_prof_categories_id', 'id');
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    public function enabled()
    {
        return $this->morphOne(Block::class, 'blockable');
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'id');
    }

    // statistics
    public function views()
    {
        return $this->morphOne(Views::class, 'viewable');
    }

    public function getViews()
    {
        if(empty($viewsModel = $this->views()->first())) {
            $viewsModel = Views::create(['count' => 0, 'viewable_id' => $this->id, 'viewable_type' => self::class]);
        }

        return $viewsModel->count;
    }

    public function getAndUpdateViews()
    {
        if(empty($viewsModel = $this->views()->first())) {
            $viewsModel = Views::create(['count' => 0, 'viewable_id' => $this->id, 'viewable_type' => self::class]);
        }
        $viewsModel->count++;
        $viewsModel->save();

        return $viewsModel->count;
    }
}
