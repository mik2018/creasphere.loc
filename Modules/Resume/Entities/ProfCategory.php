<?php

namespace Modules\Resume\Entities;

use Illuminate\Database\Eloquent\Model;

class ProfCategory extends Model
{
    protected $table = 'resume_prof_categories';
    protected $fillable = [
        'parent_id',
        'title',
    ];

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id');
    }
}
