<?php

namespace Modules\Resume\Entities;

use Illuminate\Database\Eloquent\Model;

class ResumeExperience extends Model
{
    protected $fillable = ['resume_id', 'company', 'first_day', 'last_day', 'position', 'job_tasks', 'achievements'];
}
