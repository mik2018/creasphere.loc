<?php

namespace Modules\Resume\Entities;

use Illuminate\Database\Eloquent\Model;

class ResumeEducation extends Model
{
    protected $fillable = [
        'resume_id',
        'level_id',
        'institution',
        'faculty',
        'specialty',
        'last_year',
    ];

    public function level()
    {
        return $this->belongsTo(EducationLevel::class, 'level_id');
    }
}
