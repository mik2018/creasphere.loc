<?php

namespace Modules\Resume\Entities;

use Illuminate\Database\Eloquent\Model;

class EducationLevel extends Model
{
    protected $table = 'resume_education_levels';
    protected $fillable = [];

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id');
    }
}
