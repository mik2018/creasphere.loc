<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('resume')->group(function() {
    //todo сделать нормальную страницу со списком резюме с тегами
    Route::get('/', 'ResumeController@index');
    Route::get('/{resume}', 'ResumeController@show', function (Resume $resume) {
        return $resume;
    })->name('resumes.show');
    Route::resource('/prof_categories', 'ProfCategoryController')->name('prof_categories', true)->middleware('permission:resume-pcat-edit');

//    Route::resource('/my_resume', 'PersonalResumeController')->name('my_resume', true)->middleware('auth');

});

Route::middleware(['auth'])->group(function() {
    Route::get('/my_resume/create', 'PersonalResumeController@create')->name('my_resume.create');
    Route::post('/my_resume', 'PersonalResumeController@store')->name('my_resume.store');

    Route::get('/my_resume/{my_resume}/edit', 'PersonalResumeController@edit')->name('my_resume.edit')->middleware('permission:resume-edit,my_resume');
    Route::put('/my_resume/{my_resume}', 'PersonalResumeController@update')->name('my_resume.update')->middleware('permission:resume-edit,my_resume');

    Route::post('/my_resume/{my_resume}/block', 'PersonalResumeController@block')->name('my_resume.block')->middleware('permission:resume-block');
    Route::delete('/my_resume/{my_resume}', 'PersonalResumeController@destroy')->name('my_resume.destroy')->middleware('permission:resume-delete');

    Route::get('/my_resume', 'PersonalResumeController@index')->name('my_resume.index');
    Route::get('/my_resume/{my_resume}', 'PersonalResumeController@show')->name('my_resume.show');
});


//26274 warn img-loader@3.0.1 requires a peer of imagemin@^5.0.0 || ^6.0.0 but none is installed. You must install peer dependencies yourself.
//26275 warn ajv-keywords@3.4.0 requires a peer of ajv@^6.9.1 but none is installed. You must install peer dependencies yourself.
//26276 verbose stack Error: ENOENT: no such file or directory, open '/vagrant/code/creasphere/node_modules/npm/node_modules/yargs/node_modules/y18n/package.json.1824559$
//26277 verbose cwd /vagrant/code/creasphere
//26278 verbose Linux 4.15.0-45-generic
//26279 verbose argv "/usr/bin/node" "/usr/bin/npm" "install"
//26280 verbose node v10.15.1
//26281 verbose npm  v6.9.0
//26282 error path /vagrant/code/creasphere/node_modules/npm/node_modules/yargs/node_modules/y18n/package.json.182455921
//26283 error code ENOENT
//26284 error errno -2
//26285 error syscall open
//26286 error enoent ENOENT: no such file or directory, open '/vagrant/code/creasphere/node_modules/npm/node_modules/yargs/node_modules/y18n/package.json.182455921'
//26287 error enoent This is related to npm not being able to find a file.
//26288 verbose exit [ -2, true ]
