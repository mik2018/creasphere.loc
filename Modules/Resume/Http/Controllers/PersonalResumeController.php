<?php

namespace Modules\Resume\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Resume\Entities\EducationLevel;
use Modules\Resume\Entities\Resume;
use Modules\Resume\Entities\ResumeEducation;
use Modules\Resume\Entities\ResumeExperience;
use Modules\Resume\Entities\ProfCategory;
use Image;

class PersonalResumeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('resume::myResume.index', [
            'resumes' => Resume::where('user_id', auth()->user()->id)->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('resume::myResume.create', [
            'pagename' => 'Создаём новое резюме',
            'buttonText' => 'Сохранить резюме',

            'categories' => ProfCategory::with('children')->where('parent_id', 0)->get(),
            'delimiter' => '',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'bail|required|unique:interviews|max:255',
            'image' => 'mimes:jpeg,jpg,png,gif|required'
        ]);

        $data = array_merge(
            ['user_id' => auth()->user()->id, 'image' => 'null'],
            $request->except([
                '_tocken',
                'company',
                'first_day',
                'last_day',
                'position',
                'job_tasks',
                'achievements',
                'level_id',
                'institution',
                'faculty',
                'specialty',
                'study_last_year',
            ])
        );

        $resume = Resume::create($data);

        if(!empty($request->company)) {
            for ($i = 0; $i < count($request->company); $i++)
            {
                $data = [];
                $data['resume_id'] = $resume->id;
                $data['company'] = $request->company[$i];
                $data['first_day'] = $request->first_day[$i];
                $data['last_day'] = $request->last_day[$i];
                $data['position'] = $request->position[$i];
                $data['job_tasks'] = $request->job_tasks[$i];
                $data['achievements'] = $request->achievements[$i];
                ResumeExperience::create($data);
            }
        }

        if(!empty($request->level_id)) {
            for ($i = 0; $i < count($request->level_id); $i++) {
                $data = [];
                $data['resume_id'] = $resume->id;

                $data['level_id'] = $request->level_id[$i];
                $data['institution'] = $request->institution[$i];
                $data['faculty'] = $request->faculty[$i];
                $data['specialty'] = $request->specialty[$i];
                $data['last_year'] = $request->study_last_year[$i];

                ResumeEducation::create($data);
            }
        }

        // image saving
        $image = $request->file('image');
        $imageData = Resume::prepareImagePath(public_path('img/resumes/'), $resume->id, $image->getClientOriginalExtension());

        // Image saving
        Image::make($image)->fit(800, 500)->save($imageData['imagePath'] . $imageData['imageName']);

        $dopData = ['image' => $imageData['imageName']];

        // Creating User model instance
        $resume->update($dopData);

        return redirect()->route('my_resume.index');
    }

    /**
     * Show the specified resource.
     * @param Resume $my_resume
     * @return Response
     */
    public function show(Resume $my_resume)
    {
        return view('resume::myResume.show', [
            'user' => auth()->user(),
            'resume' => $my_resume,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param Resume $my_resume
     * @return Response
     */
    public function edit(Resume $my_resume)
    {
        return view('resume::myResume.edit', [
            'resume' => $my_resume,
            'education_levels' => EducationLevel::all(),

            'categories' => ProfCategory::with('children')->where('parent_id', 0)->get(),
            'prof_category' => $my_resume->profCategory,
            'delimiter' => '',

            'buttonText' => 'Сохранить изменения'
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @param Resume $my_resume
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Resume $my_resume)
    {
        $request->validate([
            'title' => 'bail|required|unique:interviews|max:255',
            'image' => 'mimes:jpeg,jpg,png,gif'
        ]);

        $my_resume->update($request->except('_method', '_token', 'image'));

        if ($request->company && !empty($request->company)) {
            foreach ($request->company as $key => $value)
            {
//                todo прописать нормальную работу с айдишниками
                $resume = ResumeExperience::where('id', $key)->first();
                $resume->company = $request->company[$key];
                $resume->first_day = $request->first_day[$key];
                $resume->last_day = $request->last_day[$key];
                $resume->position = $request->position[$key];
                $resume->job_tasks = $request->job_tasks[$key];
                $resume->achievements = $request->achievements[$key];

                if(in_array($key, $my_resume->experiences->pluck('id')->toArray())){
                    $resume->save();
                }
            }
        }

        if ($request->level_id && !empty($request->level_id)) {
            foreach ($request->institution as $key => $value)
            {
                $education = ResumeEducation::where('id', $key)->first();
                $education->level_id = $request->level_id[$key];
                $education->institution = $request->institution[$key];
                $education->faculty = $request->faculty[$key];
                $education->specialty = $request->specialty[$key];
                $education->last_year = $request->last_year[$key];
                if(in_array($key, $my_resume->educations->pluck('id')->toArray())){
                    $education->save();
                }
            }
        }

        // image updating
        if (!empty($request->image)) {
            $image = $request->file('image');

            // Сохраняем новую картинку под старым именем
            $imagePath = Resume::prepareImagePath(public_path('img/resumes/'), $my_resume->id, $image->getClientOriginalExtension());

            // delete image if new image have a other extension
            if(!empty($my_resume->image)) {
                $extension = stristr($my_resume->image, ".", false);
                if($image->getClientOriginalExtension() !== $extension) {
                    $oldPath = public_path('img/resumes/' . $my_resume->getPathFromHash($my_resume->image));
                    $my_resume->deleteImage($oldPath);
                }
            }
            // Image saving
            Image::make($image)->fit(800, 500)->save($imagePath['imagePath'] . $imagePath['imageName']);

            $data = ['image' => $imagePath['imageName']];
            $my_resume->update($data);
        }

        return redirect()->route('my_resume.show', $my_resume);
    }

    /**
     * Remove the specified resource from storage.
     * @param Resume $my_resume
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Resume $my_resume)
    {
        $my_resume->delete();

        return redirect()->route('my_resume.index');
    }
}
