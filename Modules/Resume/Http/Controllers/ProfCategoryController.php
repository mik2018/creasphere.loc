<?php

namespace Modules\Resume\Http\Controllers;

use Modules\Resume\Entities\ProfCategory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class ProfCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('resume::prof_categories.index', [
            'categories' => ProfCategory::with('children')->where('parent_id', 0)->get(),
            'category' => [],
            'delimiter' => '',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('resume::prof_categories.create', [
            'buttonText' => 'Добавить категорию',
            'categories' => ProfCategory::with('children')->where('parent_id', 0)->get(),
            'category' => [],
            'delimiter' => '',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        ProfCategory::create($request->all());

        return redirect()->route('prof_categories.index');
    }

    /**
     * Show the specified resource.
     * @param ProfCategory $prof_category
     * @return Response
     */
    public function show(ProfCategory $prof_category)
    {
        return view('resume::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param ProfCategory $prof_category
     * @return Response
     */
    public function edit(ProfCategory $prof_category)
    {
        return view('resume::prof_categories.edit', [
            'prof_category' => $prof_category,
            'buttonText' => 'Сохранить изменения',
            'categories' => ProfCategory::with('children')->where('parent_id', 0)->get(),
            'delimiter' => '',
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @param ProfCategory $prof_category
     * @return void
     */
    public function update(Request $request, ProfCategory $prof_category)
    {
        $prof_category->update($request->all());

        return redirect()->route('prof_categories.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param ProfCategory $prof_category
     * @return Response
     * @throws \Exception
     */
    public function destroy(ProfCategory $prof_category)
    {
        $prof_category->delete();

        return redirect()->route('prof_categories.index');
    }
}
