<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupportAbusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('support_abuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type'); // Тип жалобы: Оскорбление, экстремизм, другое
            $table->string('status'); // Статус жалобы: обработано, не обработано
            $table->text('details'); // Комментарий модератора (что было сделано)
            $table->text('comment'); // комментарий того, кто пожаловался
            $table->integer('abusyable_id');    // id контента, на который пришла жалоба
            $table->string('abusyable_type');   // модель, которая управляет данным контентом

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abuses');
    }
}
