<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupportTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('support_tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject'); // тема обращения
            $table->text('description'); // описание проблемы
            $table->string('status'); // статус обращения (обработано, не обработано, др.)
            $table->text('details'); // Комментарий модератора (что было сделано)

            $table->integer('ticketable_id');
            $table->integer('ticketable_type');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('support_tickets');
    }
}
