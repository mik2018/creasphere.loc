<?php

namespace Modules\Support\Entities;

use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    protected $table = 'support_blocks';
    protected $fillable = [];
}
