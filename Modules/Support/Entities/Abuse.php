<?php

namespace Modules\Support\Entities;

use Illuminate\Database\Eloquent\Model;

class Abuse extends Model
{
    protected $table = 'support_abuses';
    protected $fillable = [];

    public function abusuable()
    {
        $this->morphTo();
    }
}
