<?php

namespace Modules\Support\Entities;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = 'support_tickets';
    protected $fillable = [];
}
