<?php

namespace Modules\Profile\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profile_roles')->insert([
            [
                'id' => 1,
                'name' => 'Administrator',
            ],
            [
                'id' => 2,
                'name' => 'Moderator',
            ],
            [
                'id' => 3,
                'name' => 'User',
            ],
        ]);
    }
}
