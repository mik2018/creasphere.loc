<?php

namespace Modules\Profile\Database\Seeders;

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Profile\Entities\Permission;

class PermissionRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <= Permission::first()->count(); $i++)
        {
            DB::table('profile_permission_role')->insert([
                [
                    'role_id' => 1,
                    'permission_id' => $i,
                ]
            ]);
        }
    }
}
