<?php

namespace Modules\Profile\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                "id" => 1,
                "name" => "Mikhail",
                "surname" => "Kobyshev",
                "patronymic" => null,
                "avatar" => "1.jpg",
                "birthday" => "2018-12-05 00:00:00",
                "city_id" => null,
                "profession" => "Web developer, web architector",
                "job" => null,
                "site" => "creasphere.ru",
                "about" => null,
                "email" => "333kmihail333@gmail.com",
                "email_verified_at" => null,
                "password" => '$2y$10$aB4zUDh4XQ5VGgr8IDBX9u7Y0H22QXBBDpzE1Gy4G2QZfJtcUykuS',
                "role_id" => 1,
                "created_at" => null,
                "updated_at" => "2019-01-07 17:17:45",
            ],
            [
                "id" => 2,
                "name" => "Елена",
                "surname" => "Глебова",
                "patronymic" => null,
                "avatar" => "",
                "birthday" => "1984-10-06 00:00:00",
                "city_id" => null,
                "profession" => null,
                "job" => null,
                "site" => "creasphere.ru",
                "about" => null,
                "email" => "ele.glebova@gmail.com",
                "email_verified_at" => null,
                "password" => '$2y$10$gDC1nrc1e2FKOZ9RW13ruucD/H62JYLqEe05A4vjmPz1C80n1sPG2',
                "role_id" => 1,
                "created_at" => null,
                "updated_at" => null,
            ],
            [
                "id" => 3,
                "name" => "YolandaAdvow",
                "surname" => null,
                "patronymic" => null,
                "avatar" => null,
                "birthday" => null,
                "city_id" => null,
                "profession" => null,
                "job" => null,
                "site" => null,
                "about" => null,
                "email" => "kakkupit@mail.ru",
                "email_verified_at" => null,
                "password" => '$2y$10$/Acvn2M35UJmtcaZMxcpzO4h8Bu6C0Pj8keWVewMBXyHMjZ4Tesk6',
                "role_id" => 3,
                "created_at" => "2019-01-24 05:02:47",
                "updated_at" => "2019-01-24 05:02:47",
            ],
            [
                "id" => 4,
                "name" => "Michaelpew",
                "surname" => null,
                "patronymic" => null,
                "avatar" => null,
                "birthday" => null,
                "city_id" => null,
                "profession" => null,
                "job" => null,
                "site" => null,
                "about" => null,
                "email" => "egorkiil@yandex.ru",
                "email_verified_at" => null,
                "password" => '$2y$10$WeNX3dTubjktAHEiJr8Cweqm4Nr1IKGMzBJE1y7.73JSquuYMt7YK',
                "role_id" => 3,
                "created_at" => "2019-01-25 10:20:04",
                "updated_at" => "2019-01-25 10:20:04",
            ],
            [
                "id" => 5,
                "name" => "sasha235079",
                "surname" => null,
                "patronymic" => null,
                "avatar" => null,
                "birthday" => null,
                "city_id" => null,
                "profession" => null,
                "job" => null,
                "site" => null,
                "about" => null,
                "email" => "putyanhinvasya@gmail.com",
                "email_verified_at" => null,
                "password" => '$2y$10$pErb09oz30XDC2ZChssQQOUH5PT7Y4nCv.hA2UmptTQcrLGz3cPn2',
                "role_id" => 3,
                "created_at" => "2019-01-25 12:33:51",
                "updated_at" => "2019-01-25 12:33:51",
            ],
            [
                "id" => 6,
                "name" => "poox",
                "surname" => null,
                "patronymic" => null,
                "avatar" => null,
                "birthday" => null,
                "city_id" => null,
                "profession" => null,
                "job" => null,
                "site" => null,
                "about" => null,
                "email" => "lidiyan3i2gor@mail.ru",
                "email_verified_at" => null,
                "password" => '$2y$10$OkH5O7hWvxC5ZTYQPQ4aH.mnkBexdqRIUwvcYwUsGRn42lHO/VSTW',
                "role_id" => 3,
                "created_at" => "2019-01-26 07:43:44",
                "updated_at" => "2019-01-26 07:43:44",
            ],
        ]);
    }
}
