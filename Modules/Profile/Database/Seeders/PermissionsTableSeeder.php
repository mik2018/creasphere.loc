<?php

namespace Modules\Profile\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profile_permissions')->insert([
            [
                "id" => 1,
                "name" => "admin-panel",
                "parent_id" => 0,
                "display_name" => "Просмотр админки",
                "description" => "",
                "created_at" => null,
                "updated_at" => null,
            ],
            [
                "id" => 2,
                "name" => "interview",
                "parent_id" => 0,
                "display_name" => "ИНТЕРВЬЮ",
                "description" => "",
                "created_at" => null,
                "updated_at" => null,
            ],
            [
                "id" => 3,
                "name" => "interview-edit",
                "parent_id" => 2,
                "display_name" => "Интервью: редактирование",
                "description" => "",
                "created_at" => null,
                "updated_at" => null,
            ],
            [
                "id" => 4,
                "name" => "interview-delete",
                "parent_id" => 2,
                "display_name" => "Интервью: удаление",
                "description" => "",
                "created_at" => null,
                "updated_at" => null,
            ],
            [
                "id" => 5,
                "name" => "interview-block",
                "parent_id" => 2,
                "display_name" => "Интервью: блокирование",
                "description" => "",
                "created_at" => null,
                "updated_at" => null,
            ],
            [
                "id" => 6,
                "name" => "material",
                "parent_id" => 0,
                "display_name" => "МАТЕРИАЛЫ",
                "description" => "",
                "created_at" => null,
                "updated_at" => null,
            ],
            [
                "id" => 7,
                "name" => "material-edit",
                "parent_id" => 6,
                "display_name" => "Материалы: редактирование",
                "description" => "",
                "created_at" => null,
                "updated_at" => null,
            ],
            [
                "id" => 8,
                "name" => "material-block",
                "parent_id" => 6,
                "display_name" => "Материалы: блокирование",
                "description" => "",
                "created_at" => null,
                "updated_at" => null,
            ],
            [
                "id" => 9,
                "name" => "material-delete",
                "parent_id" => 6,
                "display_name" => "Материалы: удаление",
                "description" => "",
                "created_at" => null,
                "updated_at" => null,
            ],
            [
                "id" => 10,
                "name" => "resume",
                "parent_id" => 0,
                "display_name" => "РЕЗЮМЕ",
                "description" => "",
                "created_at" => null,
                "updated_at" => null,
            ],
            [
                "id" => 11,
                "name" => "resume-edit",
                "parent_id" => 10,
                "display_name" => "Резюме: редактирование",
                "description" => "",
                "created_at" => null,
                "updated_at" => null,
            ],
            [
                "id" => 12,
                "name" => "resume-block",
                "parent_id" => 10,
                "display_name" => "Резюме: блокирование",
                "description" => "",
                "created_at" => null,
                "updated_at" => null,
            ],
            [
                "id" => 13,
                "name" => "resume-delete",
                "parent_id" => 10,
                "display_name" => "Резюме: удаление",
                "description" => "",
                "created_at" => null,
                "updated_at" => null,
            ],
            [
                "id" => 14,
                "name" => "resume-pcat-edit",
                "parent_id" => 10,
                "display_name" => "Резюме-проф. категории: редактирование",
                "description" => "",
                "created_at" => null,
                "updated_at" => null,
            ],
            [
                "id" => 15,
                "name" => "profile",
                "parent_id" => 0,
                "display_name" => "ПРОФИЛЬ",
                "description" => "",
                "created_at" => null,
                "updated_at" => null,
            ],
            [
                "id" => 16,
                "name" => "profile-edit",
                "parent_id" => 15,
                "display_name" => "Профиль: редактирование",
                "description" => "",
                "created_at" => null,
                "updated_at" => null,
            ],
            [
                "id" => 17,
                "name" => "profile-block",
                "parent_id" => 15,
                "display_name" => "Профиль: блокирование",
                "description" => "",
                "created_at" => null,
                "updated_at" => null,
            ],
            [
                "id" => 18,
                "name" => "profile-delete",
                "parent_id" => 15,
                "display_name" => "Профиль: удаление",
                "description" => "",
                "created_at" => null,
                "updated_at" => null,
            ],
            [
                "id" => 19,
                "name" => "profile-roles-edit",
                "parent_id" => 15,
                "display_name" => "Профиль-роли: редактирование",
                "description" => "",
                "created_at" => null,
                "updated_at" => null,
            ],
            [
                "id" => 20,
                "name" => "profile-tags-edit",
                "parent_id" => 15,
                "display_name" => "Профиль-теги: редактирование",
                "description" => "",
                "created_at" => null,
                "updated_at" => null,
            ]
        ]);
    }
}
