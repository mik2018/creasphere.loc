<?php

namespace Modules\Profile\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profile_tags')->insert([
            [
                'name' => 'фотография'
            ],
            [
                'name' => 'живопись'
            ],
        ]);
    }
}
