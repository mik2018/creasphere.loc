<?php

namespace Modules\Profile\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::table('profile_countries')->insert([
            [
                "id" => 1,
                "name" => "Украина",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 2,
                "name" => "Абхазия",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 3,
                "name" => "Австралия",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 4,
                "name" => "Австрия",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 5,
                "name" => "Азербайджан",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 6,
                "name" => "Албания",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 7,
                "name" => "Алжир",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 8,
                "name" => "Ангола",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 9,
                "name" => "Ангуилья",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 10,
                "name" => "Андорра",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 11,
                "name" => "Антигуа и Барбуда",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 12,
                "name" => "Антильские о-ва",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 13,
                "name" => "Аргентина",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 14,
                "name" => "Армения",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 15,
                "name" => "Арулько",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 16,
                "name" => "Афганистан",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 17,
                "name" => "Багамские о-ва",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 18,
                "name" => "Бангладеш",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 19,
                "name" => "Барбадос",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 20,
                "name" => "Бахрейн",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 21,
                "name" => "Беларусь",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 22,
                "name" => "Белиз",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 23,
                "name" => "Бельгия",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 24,
                "name" => "Бенин",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 25,
                "name" => "Бермуды",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 26,
                "name" => "Болгария",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 27,
                "name" => "Боливия",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 28,
                "name" => "Босния/Герцеговина",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 29,
                "name" => "Ботсвана",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 30,
                "name" => "Бразилия",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 31,
                "name" => "Британские Виргинские о-ва",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 32,
                "name" => "Бруней",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 33,
                "name" => "Буркина Фасо",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 34,
                "name" => "Бурунди",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 35,
                "name" => "Бутан",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 36,
                "name" => "Валлис и Футуна о-ва",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 37,
                "name" => "Вануату",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 38,
                "name" => "Великобритания",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 39,
                "name" => "Венгрия",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 40,
                "name" => "Венесуэла",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 41,
                "name" => "Восточный Тимор",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 42,
                "name" => "Вьетнам",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 43,
                "name" => "Габон",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 44,
                "name" => "Гаити",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 45,
                "name" => "Гайана",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 46,
                "name" => "Гамбия",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 47,
                "name" => "Гана",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 48,
                "name" => "Гваделупа",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                 "id" => 49,
                 "name" => "Гватемала",
                 "created_at" => "2018-12-20 19:35:06",
                 "updated_at" => null,
            ],
            [
                 "id" => 50,
                 "name" => "Гвинея",
                 "created_at" => "2018-12-20 19:35:06",
                 "updated_at" => null,
            ],
            [
                "id" => 51,
                "name" => "Гвинея-Бисау",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 52,
                "name" => "Германия",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 53,
                "name" => "Гернси о-в",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 54,
                "name" => "Гибралтар",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 55,
                "name" => "Гондурас",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 56,
                "name" => "Гонконг",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 57,
                "name" => "Гренада",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 58,
                "name" => "Гренландия",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 59,
                "name" => "Греция",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 60,
                "name" => "Грузия",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 61,
                "name" => "Дания",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 62,
                "name" => "Джерси о-в",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 63,
                "name" => "Джибути",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 64,
                "name" => "Доминиканская республика",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 65,
                "name" => "Египет",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 66,
                "name" => "Замбия",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 67,
                "name" => "Западная Сахара",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 68,
                "name" => "Зимбабве",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 69,
                "name" => "Израиль",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 70,
                "name" => "Индия",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 71,
                "name" => "Индонезия",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 72,
                "name" => "Иордания",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 73,
                "name" => "Ирак",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 74,
                "name" => "Иран",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 75,
                "name" => "Ирландия",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 76,
                "name" => "Исландия",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 77,
                "name" => "Испания",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 78,
                "name" => "Италия",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 79,
                "name" => "Йемен",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 80,
                "name" => "Кабо-Верде",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 81,
                "name" => "Казахстан",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 82,
                "name" => "Камбоджа",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 83,
                "name" => "Камерун",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 84,
                "name" => "Канада",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 85,
                "name" => "Катар",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 86,
                "name" => "Кения",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 87,
                "name" => "Кипр",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 88,
                "name" => "Кирибати",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 89,
                "name" => "Китай",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 90,
                "name" => "Колумбия",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 91,
                "name" => "Коморские о-ва",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 92,
                "name" => "Конго (Brazzaville)",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 93,
                "name" => "Конго (Kinshasa)",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 94,
                "name" => "Коста-Рика",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 95,
                "name" => "Кот-д''Ивуар",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 96,
                "name" => "Куба",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 97,
                "name" => "Кувейт",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 98,
                "name" => "Кука о-ва",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 99,
                "name" => "Кыргызстан",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
            [
                "id" => 100,
                "name" => "Лаос",
                "created_at" => "2018-12-20 19:35:06",
                "updated_at" => null,
            ],
        ]);
    }
}
