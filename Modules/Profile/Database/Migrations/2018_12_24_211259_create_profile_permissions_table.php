<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_permissions', function (Blueprint $table) {
            $table->increments('id'); // идентификатор
            $table->string('name')->unique(); // имя на анг.
            $table->integer('parent_id'); // родитель
            $table->string('display_name')->nullable(); // Отображаемое имя
            $table->string('description')->nullable(); // описание
            $table->index('parent_id'); // присваиваемым индекс полю родитель
            $table->timestamps();
        });

        Schema::create('profile_permission_role', function (Blueprint $table) {
            $table->integer('permission_id')->unsigned(); // id права
            $table->integer('role_id')->unsigned(); // id пользователя

            $table->foreign('permission_id')
                ->references('id')
                ->on('profile_permissions') // устанавливаем зависимости полей
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('role_id')
                ->references('id')
                ->on('profile_roles')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->primary(['permission_id', 'role_id']); // ключи
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_permissions');
        Schema::dropIfExists('profile_permission_role');
    }
}
