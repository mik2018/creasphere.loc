<?php

namespace Modules\Profile\Entities;

use Illuminate\Database\Eloquent\Model;

class StatProfile extends Model
{
    protected $table = 'profile_statistics';
    protected $fillable = ['views', 'profile_id'];

    public function getOrCreate($id): StatProfile
    {
        if(empty($profileStat = self::where('profile_id', $id)->first())) {
            $profileStat = StatProfile::create(['profile_id' => $id, 'views' => 0]);
        }
        $profileStat->views++;
        $profileStat->save();
        return $profileStat;
    }
}
