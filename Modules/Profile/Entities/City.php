<?php

namespace Modules\Profile\Entities;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'profile_cities';
    protected $fillable = [];

    public function regions()
    {
        return $this->belongsTo(Region::class);
    }
}
