<?php

namespace Modules\Profile\Entities;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = 'profile_permissions';
    protected $fillable = [];

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'profile_permission_role', 'role_id','permission_id');
    }
}
