<?php

namespace Modules\Profile\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Role extends Model
{
    protected $table = 'profile_roles';
    protected $fillable = ['name'];

    /**
     * Проверка имеет ли пользователь определенную роль
     * @param $check
     * @return boolean
     */
    public function hasPermission($check)
    {
        return in_array($check, array_pluck($this->permissions->toArray(), 'name'));
    }

    /**
     * Получает название права
     * @param $name
     * @return mixed
     */
    public function getPermissionDisplayName($name)
    {
        return DB::table('profile_permissions')->where('name', $name)->first()->display_name;
    }

    /**
     * Функция для получение прав.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'profile_permission_role', 'role_id', 'permission_id');
    }
}
