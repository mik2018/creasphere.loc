<?php

namespace Modules\Profile\Entities;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $table = 'profile_regions';
    protected $fillable = [];

    public function cities()
    {
        return $this->hasMany(City::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
