<?php

namespace Modules\Profile\Entities;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = 'profile_tags';
    protected $fillable = ['name', 'updated_at'];

    public function users()
    {
        return $this->morphedByMany('Modules\Resume\Entities\Resume', 'taggable');
    }
}
