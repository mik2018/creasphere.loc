<?php

namespace Modules\Profile\Entities;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'profile_countries';
    protected $fillable = [];

    public function regions()
    {
        return $this->hasMany(Region::class);
    }
}
