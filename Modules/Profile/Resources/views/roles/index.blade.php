@extends('layouts.app')


@section('content')
    <h1>Список доступных ролей</h1>
    <a href="{{ route('roles.create') }}">
        <button class="btn btn-outline-primary">Добавить роль</button>
    </a>

    <table class="table-bordered w-100 mt-3">
        <tr>
            <td class="p-2">
                Роль
            </td>
            <td class="p-2">
                Действия
            </td>
        </tr>
    @foreach($roles as $role)
        <tr>
            <td class="p-2">
                <a href="{{ route('roles.edit', $role) }}">
                    {{ $role->name }}
                </a>
            </td>
            <td class="p-2">
                <form class="list-inline-item" action="{{route('roles.destroy', $role)}}" onsubmit="if(confirm('Удалить?')){ return true } else{ return false }" method="post">
                    <input type="hidden" name="_method" value="delete">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-outline-primary"><i class="fa fa-trash-o"></i></button>
                </form>
            </td>
        </tr>
    @endforeach
    </table>
@stop
