@extends('layouts.app')


@section('content')
<h1>Добавляем новую роль</h1>
{!! Form::open(['route' => ['roles.store']]) !!}
    <div class="form-group">
        {{ Form::label('name', 'Имя роли') }}
        {{ Form::text('name', null, ['class' => 'form-control']) }}
    </div>
    <button role="submit">Добавить роль</button>
{!! Form::close() !!}
@stop
