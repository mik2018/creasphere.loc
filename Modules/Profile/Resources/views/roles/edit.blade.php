@extends('layouts.app')


@section('content')
    <h1>Добавляем новый тег</h1>
    {!! Form::open(['route' => ['roles.update', $role->id], 'method' => 'put']) !!}
    <div class="form-group">
        {{ Form::label('name', 'Название группы пользователей (роли)') }}
        {{ Form::text('name', $role->name, ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        <p>(заглавными буквами выделены группы прав. они отвечают за отображение пункта в меню)</p>
        @foreach($permissions as $permission)
        <div
                @if($permission->parent_id !== 0)class="ml-5"@endif
                @if(!in_array($permission->name, array_pluck($role->permissions->toArray(), 'name')))
                    style="color:#ff0000"
                @else
                    style="color:#1ab814"
                @endif()
        >
            {{ Form::checkbox("permissions[$permission->name]", $permission->id, in_array($permission->name, array_pluck($role->permissions->toArray(), 'name'))) }}
            {{ Form::label("permissions[$permission->name]", $permission->name . " ($permission->display_name)") }}
        </div>
        <br>
        @endforeach
    </div>
    <button role="submit">Сохранить</button>
    {!! Form::close() !!}
@stop
