@extends('layouts.app')

@section('content')
<h1>Добавляем новый тег</h1>
{!! Form::open(['route' => ['tags.store']]) !!}
    <div class="form-group">
        {{ Form::label('name', 'Имя тега') }}
        {{ Form::text('name', null, ['class' => 'form-control']) }}
    </div>
    <button role="submit">Добавить тег</button>
{!! Form::close() !!}
@stop
