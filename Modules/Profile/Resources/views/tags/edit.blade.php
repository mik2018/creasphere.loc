@extends('layouts.app')

@section('content')
    <h1>Добавляем новый тег</h1>
    {!! Form::open(['route' => ['tags.update', $tag->id], 'method' => 'put']) !!}
    <div class="form-group">
        {{ Form::label('name', 'Имя тега') }}
        {{ Form::text('name', $tag->name, ['class' => 'form-control']) }}
    </div>
    <button role="submit">Сохранить</button>
    {!! Form::close() !!}
@stop
