@extends('layouts.app')

@section('content')
    <h1>Список доступных тегов</h1>
    <a href="{{ route('tags.create') }}">
        <button class="btn btn-outline-primary">Добавить тег</button>
    </a>

    <table class="table-bordered w-100 mt-3">
        <tr>
            <td class="p-2">
                Тег
            </td>
            <td class="p-2">
                Удалить
            </td>
        </tr>
    @foreach($tags as $tag)
        <tr>
            <td class="p-2">
                <a href="{{ route('tags.edit', $tag) }}">
                    {{ $tag->name }}
                </a>
            </td>
            <td class="p-2">
                <form class="list-inline-item" action="{{route('tags.destroy', $tag)}}" onsubmit="if(confirm('Удалить?')){ return true } else{ return false }" method="post">
                    <input type="hidden" name="_method" value="delete">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-outline-primary"><i class="fa fa-trash-o"></i></button>
                </form>
            </td>
        </tr>
    @endforeach
    </table>
@stop
