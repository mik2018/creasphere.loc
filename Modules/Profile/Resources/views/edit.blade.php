@extends('layouts.app')

@push('topScripts')
    {{--<script src="{{ asset('js/select2.min.js') }}"></script>--}}
@endpush

@push('stylesheets')
    {{--<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />--}}
@endpush


@section('content')
    <h2>Редактирование личных данных</h2>

    {!! Form::model($user, ['route' => ['profile.update', $user], 'files' => true]) !!}
        @method('PUT')
        <div class="form-group">
            {{ Form::label('surname', 'Фамилия') }}
            {{ Form::text('surname', $user->surname, ['class' => 'form-control']) }}

            {{ Form::label('name', 'Имя') }}
            {{ Form::text('name', $user->name, ['class' => 'form-control']) }}

            {{ Form::label('patronymic', 'Отчество') }}
            {{ Form::text('patronymic', $user->patronymic, ['class' => 'form-control']) }}

            {{ Form::label('link', 'Ссыдка на страницу') }}
            {{ Form::text('link', $user->link, ['class' => 'form-control']) }}
        </div>
        <div class="form-group">
            {{ Form::label('interests', 'Укажите Ваши интересы') }}
            <select id="interests" class="js-example-basic-multiple form-control" name="interests[]" multiple="multiple">
                @foreach($tags as $tag)
                        <option value="{{ $tag->id }}"
                            @if(in_array($tag->id, $user->tags->pluck('id')->toArray(0)))
                                selected
                                @endif
                        >
                            {{ $tag->name }}
                        </option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            {{ Form::label('avatar', 'Фото профиля', ['class' => 'control-label']) }}
            {{ Form::file('avatar', null, ['class' => 'form-control']) }}
        </div>
        <div class="form-group">
            {{ Form::label('birthday', 'Дата рождения') }}
            {{ Form::date('birthday', \Carbon\Carbon::parse($user->birthday), ['class' => 'form-control']) }}
        </div>
        <div class="form-group">
            {{ Form::label('site', 'Личный сайт') }}
            {{ Form::text('site', null, ['class' => 'form-control']) }}
        </div>

        <div class="form-group">
            {{ Form::label('job', 'Место работы') }}
            {{ Form::text('job', null, ['class' => 'form-control']) }}
        </div>
        <div class="form-group">
            {{ Form::label('profession', 'Профессия') }}
            {{ Form::text('profession', null, ['class' => 'form-control']) }}
        </div>

        <div class="form-group">
            {{ Form::label('about', 'Коротко о себе') }}
            {{ Form::textarea('about', $user->about, ['class' => 'form-control', 'maxlength' => 300]) }}
        </div>
        <button class="btn btn-primary" role="submit">Сохранить</button>
    {!! Form::close() !!}
@endsection

@push('scripts')
    <script>
        $('#interests').select2({
            placeholder: "Поиск в списке интересов"
        });
    </script>
@endpush
