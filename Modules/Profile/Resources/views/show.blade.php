@extends('layouts.app')

@push('stylesheets')
    <link href="{{ asset('css/custom/profile.css') }}" rel="stylesheet">
@endpush
@section('content')
    <div class="row">
        <div class="col-md-9 col-md-push-3">
            <div class="be-large-post">
                <div class="info-block">
                    <div class="be-large-post-align">
                        {{--<span><i class="fa fa-thumbs-o-up"></i> 253</span>--}}
                        {{--<span><i class="fa fa-eye"></i> 753</span>--}}
                        {{--<span><i class="fa fa-comment-o"></i> 50</span>--}}
                    </div>
                </div>
                <div class="blog-content be-large-post-align">
                    @if($user->about)
                        <h5 class="be-post-title">
                            Коротко о себе
                        </h5>
                        <p>{!! $user->about !!}</p>
                    @endif
                    <span class="be-text-tags">
                        {{ $user->profession }}
                    </span>
                    <div class="clear"></div>

                    {{--<div class="be-largepost-iframe--}}
                    {{--embed-responsive embed-responsive-16by9">--}}
                    {{--<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/124626586" frameborder="0"></iframe>--}}
                    {{--</div>--}}
                    <div class="post-text">
                        {{ $user->about }}
                    </div>
                </div>
                <div class="be-large-post-align">
                    <h3 class="letf-menu-article">
                        Интересы
                    </h3>
                    <div class="tags_block clearfix">
                        <ul>
                            @forelse($user->tags as $tag)
                                <li><a >{{ $tag->name }}</a></li>
                            @empty
                                интересы не указаны
                            @endforelse
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row">
                <h3 class="pl-4">
                    Последние работы |
                    <a href="{{ route('materials.index', $user) }}">см. все материалы</a> |
                    <a href="{{ route('materials.create') }}">добавить новый</a>
                </h3>

                @foreach($user->materials->take(3) as $material)
                    <div class="col-md-4">
                        <div class="be-post">
                            <a href="{{ route('materials.show', $material) }}" class="be-img-block">
                                <img src="@if(!empty($material->image)){{ asset('img/materials/' . $material->getPathFromHash($material->image)) }}@endif" alt="omg">
                            </a>
                            <a href="blog-detail-2.html" class="be-post-title">{{ $material->title }}</a>
                            {{--<span>--}}
                            {{--<a href="blog-detail-2.html" class="be-post-tag">Interaction Design</a>,--}}
                            {{--<a href="blog-detail-2.html" class="be-post-tag">UI/UX</a>,--}}
                            {{--<a href="blog-detail-2.html" class="be-post-tag">Web Design</a>--}}
                            {{--</span>--}}
                            <div class="author-post">
                                @if(authAndAccess('material-edit'))
                                    <img src="{{ asset('img/p3.jpg') }}" alt="" class="ava-author">
                                    <span><a href="{{ route('materials.edit', $material) }}"><i class="fa fa-edit"></i> редактировать</a></span>
                                @endif
                            </div>
                            <div class="info-block">
                                <span><i class="fa fa-thumbs-o-up"></i> 360</span>
                                <span><i class="fa fa-eye"></i> {{ $material->getViews() }}</span>
                                <span><i class="fa fa-comment-o"></i> 20</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="col-md-3 col-md-pull-9 left-feild">
            <div class="be-user-block">
                <div class="be-user-detail">
                    <a class="be-ava-user" href="#">
                        <img class="be-ava-img" src="@if(!empty($user->avatar)){{ asset('img/users/' . $user->getPathFromHash($user->avatar)) }}@endif" alt="">
                    </a>
                    <p class="">
                        {{ $user->name }}
                    @if($user->patronymic && !empty($user->patronymic))
                        {{ $user->patronymic }}
                    @endif
                        {{ $user->surname }}
                    </p>

                    <p class="be-user-info">Дата рождения: {{ \Carbon\Carbon::parse($user->birthday)->format('d-M-y') }}</p>

                    <p class="be-user-info">Место работы: {{ $user->job }}</p>
                    <p class="be-user-info">Профессия: {{ $user->profession ?? '- / -'}}</p>
                    <p class="be-user-info">Просмотров профиля: {{ $user->getAndUpdateViews() }}</p>
                    {{--<p class="be-user-info">Singapore, Singapore</p>--}}
                </div>
                <div class="be-user-activity-block">
                    <div class="row">
                        <div class="col-lg-12">
                            @if(authAndAccess('profile-edit'))
                                <a href="{{ route('profile.edit', $user) }}" class="be-user-activity-button be-follow-type"><i class="fa fa-pencil"></i>Редактировать профиль</a>
                            @endif
                        </div>
                        {{--<div class="col-lg-12">--}}
                        {{--<a href="blog-detail-2.html" class="be-user-activity-button be-follow-type"><i class="fa fa-plus"></i>Подписаться на новости</a>--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-12">--}}
                        {{--<a href="blog-detail-2.html" class="col-lg-6 be-user-activity-button be-message-type"><i class="fa fa-envelope-o"></i>Отправить сообщение</a>--}}
                        {{--</div>--}}
                    </div>
                </div>
                {{--<h5 class="be-title">--}}
                {{--About Project--}}
                {{--</h5>--}}
                {{--<p class="be-text-userblock">--}}
                {{--Fusce dolor libero, efficitur et lobortis at, faucibus nec nunc. Proin fermentum turpis eget nisi facilisis lobortis. Praesent malesuada facilisis maximus. Donec sed lobortis tortor. Ut nec lacinia sapien, sit amet dapibus magna.--}}
                {{--</p>--}}
            </div>
            {{--<a href="blog-detail-2.html" class="be-button-vidget blue-style"><i class="fa fa-thumbs-o-up"></i>LIKE PROJECT</a>--}}
            {{--<a href="blog-detail-2.html" class="be-button-vidget grey-style"><i class="fa fa-file-o"></i>ADD TO COLLECTION</a>--}}

            <div class="resume-container">
                <h3 class="letf-menu-article text-center">Резюме</h3>
                @if(auth()->user() && auth()->user()->id === $user->id)
                    <p><a href="{{ route('my_resume.create') }}">Добавить резюме</a></p>
                @endif
                <div class="swiper-container" data-autoplay="5000" data-loop="1" data-speed="500" data-center="0" data-slides-per-view="1">
                <div class="swiper-wrapper">
                    @forelse($user->resumes->take(3) as $resume)
                        <div class="swiper-slide">
                            <div class="be-post">
                                <a href="{{ route('resumes.show', $resume) }}"class="be-img-block">
                                    <img src="@if(!empty($resume->image)){{ asset('img/resumes/' . $resume->getPathFromHash($resume->image)) }}@endif" height="202" width="269" alt="omg">
                                </a>
                                <a href="{{ route('resumes.show', $resume) }}" class="be-post-title">{{ $resume->title }}</a>
                                <span>
                                <a href="{{ route('resumes.show', $resume) }}" class="be-post-tag">Art direction</a>,
                                <a href="{{ route('resumes.show', $resume) }}" class="be-post-tag">Web Design</a>,
                                <a href="{{ route('resumes.show', $resume) }}" class="be-post-tag">Interactiob design</a>
                            </span>
                                <div class="author-post">
                                    <img src="img/ava.png" alt="" class="ava-author">
                                    <span>by <a href="{{ route('my_resume.show', $resume) }}">
                                            {{ $resume->owner->name }}
                                            {{ $resume->owner->surname }}</a>
                                    </span>

                                </div>
                                <div class="info-block">
                                    <span><i class="fa fa-thumbs-o-up"></i> 253</span>
                                    <span><i class="fa fa-eye"></i> {{ $resume->getViews() }}</span>
                                    <span><i class="fa fa-comment-o"></i> 50</span>
                                </div>
                            </div>
                        </div>
                    @empty
                        <p>Резюме не найдены</p>
                    @endforelse
                </div>
                <div class="pagination">
                </div>
            </div>
            </div>
        </div>
    </div>
@endsection