<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->group(function() {
    Route::get('/blocked', 'PersonalProfileController@blockedShow')->name('blockedProfile');

    Route::get('/my_profile', 'PersonalProfileController@show', ['name' => 'my_profile'])->name('my_profile');
//    Route::get('/my_profile/edit', 'PersonalProfileController@edit')->name('my_profile.edit');
//    Route::put('/my_profile/update', 'PersonalProfileController@update')->name('my_profile.update');

    Route::resource('/tags', 'TagController', ['name' => 'tags'])->middleware('permission:profile-tags-edit');

    // Roles
    Route::resource('/roles', 'RoleController', ['name' => 'roles'])->middleware('permission:profile-roles-edit');
});

//Route::resource('/profile', 'ProfileController', ['name' => 'profile']);
//todo сделать нормальную страницу с выводом интересных профилей
Route::get('/profile', 'ProfileController@index')->name('profile.index');
Route::get('/profile/{profile}', 'ProfileController@show')->name('profile.show');

Route::middleware(['auth'])->group(function() {
    Route::get('/profile/create', 'ProfileController@create')->name('profile.create')->middleware('permission:profile-edit');
    Route::post('/profile', 'ProfileController@store')->name('profile.store')->middleware('permission:profile-edit');

    Route::get('/profile/{profile}/edit', 'ProfileController@edit')->name('profile.edit')->middleware('permission:profile-edit,profile');
    Route::put('/profile/{profile}', 'ProfileController@update')->name('profile.update')->middleware('permission:profile-edit,profile');

    Route::post('/profile/{profile}/block', 'ProfileController@block')->name('profile.block')->middleware('permission:profile-block');
    Route::delete('/profile/{profile}', 'ProfileController@destroy')->name('profile.destroy')->middleware('permission:profile-delete');
});
