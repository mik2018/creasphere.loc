<?php

namespace Modules\Profile\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Profile\Entities\StatProfile;
use App\User as Profile;
use Modules\Profile\Entities\Tag;
use Image;

/**
 * Class ProfileController
 * @package Modules\Profile\Http\Controllers
 */
class ProfileController extends Controller
{
    public function blockedShow()
    {
        return view('profile::blockedProfile');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('profile::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('profile::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @param $id
     * @return Response
     */
    public function show($id)
    {
        if($user = User::where('id', $id)->first() or $user = User::where('link', $id)->first()) {
            return view('profile::show', [
                'user' => $user,
                'statProfile' => new StatProfile(),
            ]);
        } else {
            return abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param Profile $profile
     * @return Response
     */
    public function edit(Profile $profile)
    {
        return view('profile::edit', [
            'user' => $profile,
            'tags' => Tag::orderBy('name')->get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, Profile $profile)
    {
        $request->validate([
            'name' => 'bail|required|max:50',
            'surname' => 'bail|required|max:50',
            'patronymic' => 'bail|max:50',
            'avatar' => 'mimes:jpeg,jpg,png,gif',
        ]);

        //  Updating tags
        $profile->tags()->sync($request->interests);

        $dopData = [];
        if (!empty($request->avatar)) {
            $image = $request->file('avatar');
            $imageData = User::prepareImagePath(public_path('img/users/'), $profile->id, $image->getClientOriginalExtension());


            if(!empty($profile->avatar)) {
                // delete image if new image have a other extension
                $extension = stristr($profile->avatar, ".", false);
                if($image->getClientOriginalExtension() !== $extension) {
                    $imagePath = public_path('img/users/' . $profile->getPathFromHash($profile->avatar));
                    $profile->deleteImage($imagePath);
                }
            }

            // Image saving
            Image::make($image)->fit(200, 323)->save($imageData['imagePath'] . $imageData['imageName']);

            $dopData = ['avatar' => $imageData['imageName']];
        }
        $data = array_merge($request->except(['_token', '_method', 'avatar']), $dopData);
        $profile->update($data);

        return redirect()->route('my_profile');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
