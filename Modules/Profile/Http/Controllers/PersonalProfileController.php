<?php

namespace Modules\Profile\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Profile\Entities\Tag;
use App\User;
use Image;
use Modules\Resume\Entities\EducationLevel;

class PersonalProfileController extends Controller
{
    public function show()
    {
        return view('profile::show', [
            'user' => auth()->user(),
        ]);
    }

    public function edit()
    {
        return view('profile::edit', [
            'user' => auth()->user(),
            'tags' => Tag::orderBy('name')->get(),
        ]);
    }

    public function update(Request $request)
    {
        $request->validate([
            'name' => 'bail|required|max:50',
            'surname' => 'bail|required|max:50',
            'patronymic' => 'bail|max:50',
            'avatar' => 'mimes:jpeg,jpg,png,gif',
        ]);

        //  Updating tags
        $user = auth()->user();
        $user->tags()->sync($request->interests);

        $dopData = [];
        if (!empty($request->avatar)) {
            $image = $request->file('avatar');
            $imageData = User::prepareImagePath(public_path('img/users/'), $user->id, $image->getClientOriginalExtension());


            if(!empty($user->avatar)) {
                // delete image if new image have a other extension
                $extension = stristr($user->avatar, ".", false);
                if($image->getClientOriginalExtension() !== $extension) {
                    $imagePath = public_path('img/users/' . $user->getPathFromHash($user->avatar));
                    $user->deleteImage($imagePath);
                }
            }

            // Image saving
            Image::make($image)->fit(200, 323)->save($imageData['imagePath'] . $imageData['imageName']);

            $dopData = ['avatar' => $imageData['imageName']];
        }
        $data = array_merge($request->except(['_token', '_method', 'avatar']), $dopData);
        $user->update($data);

        return redirect()->route('my_profile');
    }
}
