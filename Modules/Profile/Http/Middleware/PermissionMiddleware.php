<?php

namespace Modules\Profile\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class PermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request $request
     * @param  \Closure $next
     * @param $permission
     * @return mixed
     */
    public function handle($request, Closure $next, $permission, $model = null)
    {
        if(!empty($model)) {
            $ownerId = request()->route($model)->id;
            if($ownerId === auth()->user()->id) {
                return $next($request);
            }
        }

        if(!$request->user()->hasPermission($permission)) {
            return redirect()->route('home');
        }

        return $next($request);
    }
}
