<?php

namespace Modules\Statistics\Entities;

use Illuminate\Database\Eloquent\Model;

class Views extends Model
{
    protected $table = 'statistics_views';
    protected $fillable = ['count', 'viewable_id', 'viewable_type'];
}
