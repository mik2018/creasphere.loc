<?php

namespace Modules\Material\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Material\Entities\Material;
use Image;

class MaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param null $userId
     * @return Response
     */
    public function index($userId = null)
    {
        if(empty($userId)) {
            $materials = Material::where('user_id', auth()->user()->id)->paginate(20);
        } else{
            $materials = Material::where('user_id', $userId)->paginate(20);
        }
        return view('material::index',[
            'materials' => $materials
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('material::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'bail|required|unique:materials|max:255',
            'image' => 'mimes:jpeg,jpg,png,gif|required',
        ]);

        $data = array_merge($request->except('image', '_token'), ['user_id' => auth()->user()->id]);
        $material = Material::create($data);

        $image = $request->file('image');
        $imageData = Material::prepareImagePath(public_path('img/materials/'), $material->id, $image->getClientOriginalExtension());

        // Image saving
        Image::make($image)->fit(800, 500)->save($imageData['imagePath'] . $imageData['imageName']);

        $dopData = ['image' => $imageData['imageName']];

        // Creating User model instance
        $material->update($dopData);

        return redirect()->route('my_profile');
    }

    /**
     * Show the specified resource.
     * @param Material $material
     * @return Response
     */
    public function show(Material $material)
    {
        return view('material::show', [
            'material' => $material
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param Material $material
     * @return Response
     */
    public function edit(Material $material)
    {
        // todo дыра в редактировании. нужно проверять наличие авторства для открытия доступа.
        // todo проблема доступа редактирования при блокировке доступа в роутах.
        if(auth()->user()->hasPermission('sdfsdf') or auth()->user()->id === $material->user_id){

        }

        return view('material::edit', [
            'material' => $material
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @param Material $material
     * @return Response
     */
    public function update(Request $request, Material $material)
    {
        $request->validate([
            'title' => 'bail|required|max:255',
            'image' => 'mimes:jpeg,jpg,png,gif',
        ]);

        $dopData = [];

        if(!empty($request->image)) {
            $image = $request->file('image');
            $imageData = Material::prepareImagePath(public_path('img/materials/'), $material->id, $image->getClientOriginalExtension());

            // delete image if new image have a other extension
            $extension = stristr($material->image, ".", false);
            if($image->getClientOriginalExtension() !== $extension) {
                $imagePath = public_path('img/materials/' . $material->getPathFromHash($material->image));
                $material->deleteImage($imagePath);
            }

            // Image saving
            Image::make($image)->fit(800, 500)->save($imageData['imagePath'] . $imageData['imageName']);

            $dopData = ['image' => $imageData['imageName']];
        }

        $material->update(array_merge($request->except('image'), $dopData));

        return redirect()->route('my_profile');
    }

    /**
     * Remove the specified resource from storage.
     * @param Material $material
     * @return Response
     * @throws \Exception
     */
    public function destroy(Material $material)
    {
        $imagePath = public_path('img/materials/' . $material->getPathFromHash($material->image));
        $material->deleteImage($imagePath);
        $material->delete();

        return redirect()->route('my_profile');
    }
}
