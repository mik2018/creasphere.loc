<?php

namespace Modules\Material\Entities;

use App\Traits\ImageTrait;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Modules\Statistics\Entities\Views;

class Material extends Model
{
    use ImageTrait;

    protected $fillable = ['title', 'description', 'user_id', 'image'];

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    // statistics
    public function views()
    {
        return $this->morphOne(Views::class, 'viewable');
    }

    public function getViews()
    {
        if(empty($viewsModel = $this->views()->first())) {
            $viewsModel = Views::create(['count' => 0, 'viewable_id' => $this->id, 'viewable_type' => self::class]);
        }

        return $viewsModel->count;
    }

    public function getAndUpdateViews()
    {
        if(empty($viewsModel = $this->views()->first())) {
            $viewsModel = Views::create(['count' => 0, 'viewable_id' => $this->id, 'viewable_type' => self::class]);
        }
        $viewsModel->count++;
        $viewsModel->save();

        return $viewsModel->count;
    }
}
