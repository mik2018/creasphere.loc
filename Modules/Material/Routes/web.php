<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::middleware(['auth'])->group(function() {
//    Route::resource('/materials', 'MaterialController');
    Route::get('/materials/create', 'MaterialController@create')->name('materials.create')->middleware('permission:material');
    Route::post('/materials', 'MaterialController@store')->name('materials.store')->middleware('permission:material');

    Route::get('/materials/{material}/edit', 'MaterialController@edit')->name('materials.edit')->middleware('permission:material-edit,material');
    Route::put('/materials/{material}', 'MaterialController@update')->name('materials.update')->middleware('permission:material-edit,material');

    Route::post('/materials/{material}/block', 'MaterialController@block')->name('materials.block')->middleware('permission:material-block');
    Route::delete('/materials/{material}', 'MaterialController@destroy')->name('materials.destroy')->middleware('permission:material-delete,material');

    Route::get('/materials/{userId?}/all', 'MaterialController@index', function ($userId = null) {
        return $userId;
    })->name('materials.index');
});

Route::get('/materials/{material}', 'MaterialController@show')->name('materials.show');
