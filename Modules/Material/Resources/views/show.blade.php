@extends('layouts.app')

@push('stylesheets')
    <link href="{{ asset('css/custom/material.css') }}" rel="stylesheet">
@endpush
@section('content')
    <h1>Материал: {{ $material->title }}</h1>
    <p>Владелец: {{ $material->owner->name }}</p>
    <img class="material-img" src="@if(!empty($material->image)){{ asset('img/materials/' . $material->getPathFromHash($material->image)) }}@endif" alt="">

    <p>
        {{ $material->description }}
    </p>

    <div class="clearfix"></div>
    <p>
        <span><i class="fa fa-eye"></i> {{ $material->getAndUpdateViews() }}</span>
    </p>
@stop
