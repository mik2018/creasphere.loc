<div class="form-group">
    {{ Form::label('title', 'Название материала') }}
    {{ Form::text('title', null, ['class' => 'form-control', 'required']) }}
</div>
<div class="form-group">
    {{ Form::label('description', 'Описание') }}
    {{ Form::textarea('description', null, ['class' => 'form-control', 'required']) }}
</div>
<div class="form-group">
    {{ Form::label('image', 'Загрузите картинки', ['class' => 'control-label', 'required']) }}
    {{ Form::file('image', null, ['class' => 'form-control']) }}
</div>