@extends('layouts.app')


@section('content')
    <h1>Редактируем материал</h1>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::model($material, ['route' => ['materials.update', $material], 'method' => 'put', 'files' => true]) !!}
    @include('material::_form')
    <button class="btn btn-primary" role="submit">Сохранить изменения</button>
    {!! Form::close() !!}
@stop
