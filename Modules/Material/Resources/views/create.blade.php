@extends('layouts.app')


@section('content')
    <h1>Добавляем новый материал</h1>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(['route' => ['materials.store'], 'files' => true]) !!}
    @include('material::_form')
    <button class="btn btn-primary" role="submit">Добавить материал</button>
    {!! Form::close() !!}
@stop
