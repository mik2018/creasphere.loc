@extends('layouts.app')

@push('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/custom/material.css') }}">
@endpush
@section('content')
    <h1>Материалы</h1>
    <div class="container materials">
        <div class="row">
            @forelse($materials as $material)
                <div class="col-sm-3 material-item">
                    <a href="{{ route('materials.edit', $material) }}">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                        {{ $material->title }}
                    </a>
                    @if(auth()->user()->hasPermission('material-delete'))
                        <form class="list-inline-item" action="{{route('materials.destroy', $material)}}" onsubmit="if(confirm('Удалить?')){ return true } else{ return false }" method="post">
                            <input type="hidden" name="_method" value="delete">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-outline-primary"><i class="fa fa-trash-o"></i></button>
                        </form>
                    @endif
                    <p>
                        <img src="@if(!empty($material->image)){{ asset('img/materials/' . $material->getPathFromHash($material->image)) }}@endif" alt="">
                    </p>
                </div>
            @empty
                <h4>Список материалов пуст</h4>
            @endforelse
        </div>
    </div>
@stop
