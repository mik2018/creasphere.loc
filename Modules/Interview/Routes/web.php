<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/interviews', 'InterviewController@index')->name('interviews.index');

Route::get('/interviews/order/create', 'InterviewController@createOrder')->name('interviews.order.create');

Route::get('/interviews/create', 'InterviewController@create')->name('interviews.create')->middleware('permission:interview');
Route::post('/interviews', 'InterviewController@store')->name('interviews.store')->middleware('permission:interview');

Route::get('/interviews/{interview}', 'InterviewController@show')->name('interviews.show');
Route::get('/interviews/{interview}/edit', 'InterviewController@edit')->name('interviews.edit')->middleware('permission:interview-edit');
Route::put('/interviews/{interview}', 'InterviewController@update')->name('interviews.update')->middleware('permission:interview-edit');

Route::post('/interviews/{interview}/block', 'InterviewController@block')->name('interviews.block')->middleware('permission:interview-block');
Route::delete('/interviews/{interview}', 'InterviewController@destroy')->name('interviews.destroy')->middleware('permission:interview-delete');

Route::get('/admin/interviews', 'InterviewController@adminIndex')->name('admin.interviews.index', true);
