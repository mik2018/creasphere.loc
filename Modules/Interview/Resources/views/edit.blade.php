@extends('layouts.app')


@section('content')
    <h1>Редактируем интервью</h1>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::model($interview, ['route' => ['interviews.update', $interview], 'method' => 'put', 'files' => true]) !!}
    @include('interview::_form')
    <button class="btn btn-primary" role="submit">Сохранить изменения</button>
    {!! Form::close() !!}
@stop
