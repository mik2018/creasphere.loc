@extends('layouts.app')


@section('content')
    <h1>Интервью</h1>
    <br>
    <a href="{{ route('interviews.create') }}"><button class="btn btn-primary"><i class="fa fa-plus"></i>Добавить интервью</button></a>
    <br>
    <br>
    <table class="w-100 table-bordered">
        <tr>
            <td class="p-3">Интервью</td>
            <td class="p-3">Доступные действия</td>
        </tr>
        @forelse($interviews as $interview)
        <tr>
            <td class="p-3">{{ $interview->title }}</td>
            <td class="p-3">
                <a href="{{ route('interviews.edit', $interview) }}" name="Редактировать">{{ $interview->title }}</a>
                @if (auth()->user()->hasPermission('interview-delete'))
                <form class="list-inline-item" action="{{route('interviews.destroy', $interview)}}" onsubmit="if(confirm('Удалить?')){ return true } else{ return false }" method="post">
                    <input type="hidden" name="_method" value="delete">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-outline-primary"><i class="fa fa-trash-o"></i></button>
                </form>
                @endif
            </td>
        </tr>
        @empty
        <tr>
            <td class="p-3" colspan="2">Интервью не нaйдены</td>
        </tr>
    @endforelse
    </table>
    {{ $interviews->links() }}
@stop
