<div class="form-group">
    {{ Form::label('title', 'Название интервью') }}
    {{ Form::text('title', null, ['class' => 'form-control', 'required']) }}
</div>
<div class="form-group">
    {{ Form::label('meta_description', 'Мета описание') }}
    {{ Form::textarea('meta_description', null, ['class' => 'form-control', 'rows' => 3, 'required']) }}
</div>
<div class="form-group">
    {{ Form::label('meta_keywords', 'Мета ключевые слова') }}
    {{ Form::text('meta_keywords', null, ['class' => 'form-control', 'required']) }}
</div>
<hr>
<div class="form-group">
    {{ Form::label('journalist_id', 'Интервьер') }}
    <select name="journalist_id" id="journalist_id">
        @foreach($users as $user)
        <option
            value="{{ $user->id }}"
            @if(isset($interview) && $user->id == $interview->journalist_id) selected @endif
        >{{ $user->name }} {{ $user->surname }} ({{ $user->role->name }})</option>
        @endforeach
    </select>
</div>
<hr>
<div class="form-group">
    {{ Form::label('person_name', 'Интервьюируемый(ФИО)') }}
    {{ Form::text('person_name', null, ['class' => 'form-control' , 'required']) }}
</div>
<div class="form-group">
    {{ Form::label('person_id', 'Профиль интервьюируемого (если есть)') }}
    <select name="person_id" id="person_id" class="form-control">
        <option value="">Не указан</option>
        @foreach($users as $user)
        <option
            value="{{ $user->id }}"
            @if(isset($interview) && $user->id == $interview->person_id) selected @endif
        >{{ $user->name }} {{ $user->surname }} ({{ $user->role->name }})</option>
        @endforeach
    </select>
</div>
<hr>
<div class="form-group">
    {{ Form::label('image', 'Загрузите картинку', ['class' => 'control-label', 'multiple']) }}
    {{ Form::file('image', null, ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('description', 'Описание') }}
    {{ Form::textarea('editor', null, ['class' => 'form-control', 'required']) }}
</div>
<div class="form-group">
    {{ Form::label('content', 'Контент') }}
    {{ Form::textarea('content', null, ['class' => 'form-control', 'required']) }}
</div>
