@extends('layouts.app')

@section('content')
    <div class="isotope-grid row">
        @forelse($interviews as $interview)
            <div class="isotope-item col-ml-12 col-xs-6 col-sm-4 col-md-3 col-lg-3 col-xl-3">
                <div class="be-post style-5">
                    <a href="{{ route('interviews.show', $interview) }}" class="be-img-block">
                        <img src="@if(!empty($interview->image)){{ asset('img/interviews/' . $interview->getPathFromHash($interview->image)) }}@endif" alt="omg">
                    </a>
                    {{--<div class="be-rowline">--}}
                        {{--<img class="rowline-img" src="{{ 'img/users/' . ($interview->person->avatar ?? '../line_1.jpg') }}" alt="">--}}
                        {{--<div class="rowline-text">Louis Paquet <span class="rowline-icon"><i class="fa fa-thumbs-o-up"></i></span></div>--}}
                    {{--</div>--}}
                    <div class="be-rowline">
                        <div class="rowline-text">
                            {{ $interview->title }}
                            @if(auth()->user() && auth()->user()->hasPermission('interview-edit'))
                                <a href="{{ route('interviews.edit', $interview) }}">
                                    <span class="rowline-icon"><i class="fa fa-pencil"></i></span>
                                </a>
                            @endif
                            <br>
                            <p>Добавлено {{ $interview->created_at }}</p>
                            <p><span class=""><i class="fa fa-eye"></i> {{ $interview->getViews() }}</span> </p>
                        </div>
                    </div>
                    <div class="be-rowline">
                        <a href="@if($interview->person){{ route('profile.show', $interview->person) }}@endif">
                            <img class="rowline-img" @if($interview->person)src="{{ asset('img/users/' . $interview->getPathFromHash($interview->person->avatar)) }}" @else src="img/line_2.jpg" @endif alt="">
                            <div class="rowline-text">Герой интервью: {{ $interview->person_name }} </div>
                        </a>
                    </div>
                    <div class="author-post">
                        <a href="@if($interview->journalist){{ route('profile.show', $interview->journalist->getLink()) }}@endif">
                            <img @if($interview->journalist)src="img/avs/{{ $interview->journalist->avatar }}" @elsesrc="img/line_2.jpg" @endif alt="" class="ava-author">
                            <span>
                                Автор:
                                    @if($interview->journalist->name && $interview->journalist->surname)
                                        {{ $interview->journalist->name . ' ' . $interview->journalist->surname }}
                                    @else
                                        {{ $interview->journalist->name }}
                                    @endif
                            </span>
                            @if(auth()->user() && auth()->user()->hasPermission('interview-edit'))
                                <span class="pull-right"><i class="fa fa-thumbs-o-up"></i> 225</span>
                            @endif
                        </a>
                    </div>
                </div>
            </div>
        @empty
            <p>Интервью не найдены</p>
        @endforelse
    </div>
    <div class="clearfix"></div>
    <a href="{{ route('interviews.order.create') }}">
        <button class="btn btn-danger">Хочу своё интервью!</button>
    </a>
@stop
