@extends('layouts.app')

@section('content')
    <h1>Добавляем новое интервью</h1>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::open(['route' => ['interviews.store'], 'files' => true]) !!}
    @include('interview::_form')
    <button class="btn btn-primary" role="submit">Добавить интервью</button>
    {!! Form::close() !!}
@stop
