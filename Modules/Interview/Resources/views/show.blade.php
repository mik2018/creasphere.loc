@extends('layouts.app')


@push('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/custom/interview.css') }}">
@endpush
@section('content')
    <h1>{{ $interview->title }}</h1>
    <p>Автор: {{ $interview->journalist->name }}</p>
    <p>
        <img src="{{ asset('img/interviews/' . $interview->getPathFromHash($interview->image)) }}" alt="omg">
    </p>

    <div>
        {!! $interview->description !!}
    </div>
    <div>
        {!! $interview->content !!}
    </div>
    <div>
        <p>
            <span><i class="fa fa-thumbs-o-up"></i> 360</span>
            <span><i class="fa fa-eye"></i> {{ $interview->getAndUpdateViews() }}</span>
            <span><i class="fa fa-comment-o"></i> 20</span>
        </p>
    </div>
@stop
