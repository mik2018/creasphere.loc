<?php

namespace Modules\Interview\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Interview\Entities\Interview;
use Image;

class InterviewController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('interview::index', [
            'interviews' => Interview::paginate(15)
        ]);
    }

    /**
     * Display a listing of the resource. Only for admins
     * @return Response
     */
    public function adminIndex()
    {
        return view('interview::admin.index', [
            'interviews' => Interview::paginate(15)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('interview::create', [
            'users' => User::all(),
        ]);
    }

    public function createOrder()
    {
        return view('interview::order.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'bail|required|unique:interviews|max:255',
            'image' => 'mimes:jpeg,jpg,png,gif|required',
            'meta_description' => 'bail|required',
            'meta_keywords' => 'bail|required',
            'journalist_id' => 'bail|integer|required',
            'person_name' => 'bail|required|max:255',
        ]);

        $interview = Interview::create($request->except(['_token', 'image']));

        if (!empty($request->image)) {
            $image = $request->file('image');
            $imageData = Interview::prepareImagePath(public_path('img/interviews/'), $interview->id, $image->getClientOriginalExtension());

            // Image saving
            Image::make($image)->fit(800, 500)->save($imageData['imagePath'] . $imageData['imageName']);

            $dopData = ['image' => $imageData['imageName']];

            // Creating User model instance
            $interview->update($dopData);
        }

        return redirect()->route('admin.interviews.index');
    }

    /**
     * Show the specified resource.
     * @param Interview $interview
     * @return Response
     */
    public function show(Interview $interview)
    {
        return view('interview::show', [
            'interview' => $interview,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param Interview $interview
     * @return Response
     */
    public function edit(Interview $interview)
    {
        return view('interview::edit', [
            'interview' => $interview,
            'users' => User::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @param Interview $interview
     * @return Response
     */
    public function update(Request $request, Interview $interview)
    {
        $request->validate([
            'title' => 'bail|required|max:255',
            'image' => 'mimes:jpeg,jpg,png,gif',
            'meta_description' => 'bail|required',
            'meta_keywords' => 'bail|required',
            'journalist_id' => 'bail|integer|required',
            'person_name' => 'bail|required|max:255',
        ]);

        // не удалять картинку, а заменять её на новую
        $data = $request->except(['image']);
        // при пустом значении в картинке генерируем хеш с айдишника
        if (!empty($request->image)) {
            $image = $request->file('image');

            // Сохраняем новую картинку под старым именем
            $imagePath = Interview::prepareImagePath(public_path('img/interviews/'), $interview->id, $image->getClientOriginalExtension());

            // delete image if new image have a other extension
            $extension = stristr($interview->image, ".", false);
            if($image->getClientOriginalExtension() !== $extension) {
                $oldPath = public_path('img/interviews/' . $interview->getPathFromHash($interview->image));
                $interview->deleteImage($oldPath);
            }

            // Image saving
            Image::make($image)->fit(800, 500)->save($imagePath['imagePath'] . $imagePath['imageName']);

            $data = array_merge($request->except(['_token', '_method', 'image']), ['image' => $imagePath['imageName']]);
        }

        $interview->update($data);

        return redirect()->route('admin.interviews.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param Interview $interview
     * @return Response
     * @throws \Exception
     */
    public function destroy(Interview $interview)
    {
        // todo прописать тело медода deleteImage()
        $imagePath = public_path('img/interviews/' . $interview->getPathFromHash($interview->image));
        $interview->deleteImage($imagePath);
        $interview->delete();

        return redirect()->route('admin.interviews.index');
    }
}
