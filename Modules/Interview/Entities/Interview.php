<?php

namespace Modules\Interview\Entities;

use App\Traits\DirectoryTrait;
use App\Traits\ImageTrait;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\Statistics\Entities\Views;

class Interview extends Model
{
    use ImageTrait;

    protected $fillable = ['title', 'meta_description', 'meta_keywords', 'image', 'journalist_id', 'person_name', 'person_id', 'description', 'content'];

    public function person()
    {
        return $this->belongsTo(User::class, 'person_id');
    }

    public function journalist()
    {
        return $this->belongsTo(User::class, 'journalist_id');
    }

    // statistics
    public function views()
    {
        return $this->morphOne(Views::class, 'viewable');
    }

    public function getViews()
    {
        if(empty($viewsModel = $this->views()->first())) {
            $viewsModel = Views::create(['count' => 0, 'viewable_id' => $this->id, 'viewable_type' => self::class]);
        }

        return $viewsModel->count;
    }

    public function getAndUpdateViews()
    {
        if(empty($viewsModel = $this->views()->first())) {
            $viewsModel = Views::create(['count' => 0, 'viewable_id' => $this->id, 'viewable_type' => self::class]);
        }
        $viewsModel->count++;
        $viewsModel->save();

        return $viewsModel->count;
    }
}
