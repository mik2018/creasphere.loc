<?php

use Illuminate\Database\Seeder;

use Modules\Profile\Database\Seeders\ProfileDatabaseSeeder;
use Modules\Resume\Database\Seeders\ResumeDatabaseSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // вызов сидеров модуля Profile
         $this->call(ProfileDatabaseSeeder::class);

         // Calling seeders Resumes module
         $this->call(ResumeDatabaseSeeder::class);
    }
}
