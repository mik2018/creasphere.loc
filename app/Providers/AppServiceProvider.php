<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('enabled', function (string $expression): string {
            // Chose the role of the current user
            return "
                <?php
                    if ($expression):
                ?>
            ";
        });
        /**
         * EndRole blade dirrective
         *
         * @param  string  $expression
         * @return string
         */
        Blade::directive('endenabled', function($expression): string {
            return '<?php
                    endif;
                ?>
            ';
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
