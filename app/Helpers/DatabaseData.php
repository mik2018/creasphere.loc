<?php
/**
 * Created by PhpStorm.
 * User: Михаил
 * Date: 22.12.2018
 * Time: 21:52
 */

namespace App\Helpers;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class DatabaseData
{
    public function getData($table)
    {
        return collect(DB::table($table)->get())->toArray();
    }

    public function saveData($data)
    {
        $data = serialize($data);
        Storage::disk('public')->put('db/region.json', $data);
        dd('ok');
    }
}