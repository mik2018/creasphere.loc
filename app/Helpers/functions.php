<?php

if (! function_exists('authAndAccess')) {
    function authAndAccess($permission) {
        if(auth()->user() === null) return false;
        return auth()->user()->hasPermission($permission);
    }
}