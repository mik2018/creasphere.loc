<?php

namespace App;

use App\Traits\ImageTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Modules\Profile\Entities\Role;
use Modules\Profile\Entities\Tag;
use Modules\Material\Entities\Material;
use Modules\Resume\Entities\Resume;
use Modules\Statistics\Entities\Views;

class User extends Authenticatable
{
    use Notifiable;
    use ImageTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'surname',
        'patronymic',
        'avatar',
        'link',
        'birthday',
        'city_id',
        'site',
        'email',
        'password',
        'job',
        'profession',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    /**
     * Profile module relationships
     */
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'profile_taggable');
    }

    public function role()
    {
        return $this->hasOne(Role::class, 'id', 'role_id');
    }

    public function hasPermission($role)
    {
        return $this->role->hasPermission($role);
    }

    public function getPermissionDisplayName($name)
    {
        // todo раскоментить и дописать логику
//        return $this->role->getPermissionDisplayName($name) or $this->materialPermission === 1;
        return $this->role->getPermissionDisplayName($name);
    }

    public function getLink()
    {
        if(!empty($this->link)){
            return $this->link;
        } else {
            return $this;
        }
    }

    /**
     * Material module relationships
     */
    public function materials()
    {
        return $this->hasMany(Material::class);
    }

    public function isBlocked()
    {
//        ($this->morphOne(Block::class, 'blockable') instanceof Block) ? return true : return false;
    }

    public function resumes()
    {
        return $this->hasMany(Resume::class, 'id');
    }

    // statistics
    public function views()
    {
        return $this->morphOne(Views::class, 'viewable');
    }

    public function getAndUpdateViews()
    {
        if(empty($viewsModel = $this->views()->first())) {
            $viewsModel = Views::create(['count' => 0, 'viewable_id' => $this->id, 'viewable_type' => self::class]);
        }
        $viewsModel->count++;
        $viewsModel->save();

        return $viewsModel->count;
    }
}
