<?php

namespace App\Traits;


trait ImageTrait
{
    // сформировать путь для сохранения
    public static function prepareImagePath ($path, $imageName, $extension): iterable
    {
        for($i = strlen($imageName); $i < 11; $i++){
            $imageName = '0' . $imageName;
        }

        $imageHash = md5($imageName);

        $imageName = $imageHash . '.';
        $imageName .= $extension;

        $imagePath = $path;
        $imagePath .= substr($imageHash, 0, 3);
        if(!file_exists($imagePath)) mkdir($imagePath);
        $imagePath .= "/";
        $imagePath .= substr($imageHash, 3, 1);
        if(!file_exists($imagePath)) mkdir($imagePath);
        $imagePath .= "/";
        $imagePath .= substr($imageHash, 4, 1);
        if(!file_exists($imagePath)) mkdir($imagePath);
        $imagePath .= "/";

        return ['imageName' => $imageName, 'imagePath' => $imagePath];
    }

    public static function getPathFromHash(?string $fileName): string
    {
        $path = substr($fileName, 0, 3);
        $path .= '/';
        $path .= substr($fileName, 3, 1);
        $path .= '/';
        $path .= substr($fileName, 4, 1);
        $path .= '/';
        $path .= $fileName;

        return $path;
    }

    public static function deleteImage(?string $imagePath): bool
    {
        $result = false;
        if(file_exists($imagePath)) {
            $result = unlink($imagePath);
        }

        return $result;
    }
}