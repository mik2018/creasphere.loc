<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Неверный логин или пароль',
    'throttle' => 'Слишком много попыток входа в систему. Пожалуйста, попробуйте снова через :seconds секунд.',

    'Login' => 'Вход на сайт',
    'email' => 'E-Mail',
    'password' => 'Пароль',
    'remember-me' => 'Запомнить меня',
    'forgot-password' => 'Забыли пароль?',
    'register-button' => 'Зарегистрироваться',
    'login-button' => 'Войти',
];
