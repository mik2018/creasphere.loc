<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Креасфера</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    {{--font awesome icons--}}
    <script src="https://use.fontawesome.com/ec33f82de0.js"></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
    {{--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">--}}

    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/icon.css') }}">
    <link rel="stylesheet" href="{{ asset('css/loader.css') }}">
    <link rel="stylesheet" href="{{ asset('css/idangerous.swiper.css') }}">
    <link rel="stylesheet" href="{{ asset('css/stylesheet.css') }}">

    <!--[if lt IE 10]>
        <link rel="stylesheet" type="text/css" href="{{ asset('css/ie-9.css') }}" />
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom/responsive.css') }}" rel="stylesheet">
    @stack('stylesheets')


<!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js" async></script>
    <script src="{{ asset('js/app.js') }}" async></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

    @stack('topScripts')

</head>
<body>
    {{--<!-- THE LOADER -->--}}
    {{--<div class="be-loader">--}}
        {{--<div class="spinner">--}}
            {{--<div class="spinner-container container1">--}}
                {{--<div class="circle1"></div>--}}
                {{--<div class="circle2"></div>--}}
                {{--<div class="circle3"></div>--}}
                {{--<div class="circle4"></div>--}}
            {{--</div>--}}
            {{--<div class="spinner-container container2">--}}
                {{--<div class="circle1"></div>--}}
                {{--<div class="circle2"></div>--}}
                {{--<div class="circle3"></div>--}}
                {{--<div class="circle4"></div>--}}
            {{--</div>--}}
            {{--<div class="spinner-container container3">--}}
                {{--<div class="circle1"></div>--}}
                {{--<div class="circle2"></div>--}}
                {{--<div class="circle3"></div>--}}
                {{--<div class="circle4"></div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    @include('layouts.partials.header')

    <div id="content-block">
        {{--<main class="py-4 container">--}}
        <div class="container custom-container be-detail-container">
            @yield('content')
        </div>
        {{--</main>--}}
    </div>

    @include('layouts.partials.footer')

    <!-- SCRIPTS     -->

    {{--<script src="{{ asset('js/jquery-3.3.1.js') }}"></script>--}}
    {{--<script src="{{ asset('js/bootstrap.min.js') }}"></script>--}}
    <script src="{{ asset('js/idangerous.swiper.min.js') }}"></script>
    <script src="{{ asset('js/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('js/jquery.viewportchecker.min.js') }}"></script>
    <script src="{{ asset('js/global.js') }}"></script>
    @stack('scripts')

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(52783885, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
        });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/52783885" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
</body>
</html>
