<!-- THE HEADER -->
<header>
    <div class="container-fluid custom-container">
        <div class="row no_row row-header">
            <div class="brand-be">
                <a href="{{ route('home') }}"><img src="{{ asset('img/logo.png') }}" alt="logo" class="be_logo"></a>
                <p>Креативный нетворкинг</p>
            </div>
            @auth
            <div class="login-header-block">
                <div class="login_block">
                    {{--<a class="notofications-popup" href="blog-detail-2.html">--}}
                        {{--<i class="fa fa-bell-o"></i>--}}
                        {{--<span class="noto-count">23</span>--}}
                    {{--</a>--}}
                    <div class="noto-popup notofications-block">
                        <div class="noto-label">Your Notification</div>
                        <div class="noto-body">
                            <div class="noto-entry">
                                <div class="noto-content clearfix">
                                    <div class="noto-img">
                                        <a href="blog-detail-2.html">
                                            <img src="{{ asset('img/c1.png') }}" alt="" class="be-ava-comment">
                                        </a>
                                    </div>
                                    <div class="noto-text">
                                        <div class="noto-text-top">
                                            <span class="noto-name"><a href="blog-detail-2.html">Ravi Sah</a></span>
                                            <span class="noto-date"><i class="fa fa-clock-o"></i> May 27, 2015</span>
                                        </div>
                                        <a href="blog-detail-2.html" class="noto-message">Start following your work</a>
                                    </div>
                                </div>
                            </div>
                            <div class="noto-entry">
                                <div class="noto-content clearfix">
                                    <div class="noto-img">
                                        <a href="blog-detail-2.html">
                                            <img src="{{ asset('img/c6.jpg') }}" alt="" class="be-ava-comment">
                                        </a>
                                    </div>
                                    <div class="noto-text">
                                        <div class="noto-text-top">
                                            <span class="noto-name"><a href="blog-detail-2.html">Louis Paquet</a></span>
                                            <span class="noto-date"><i class="fa fa-clock-o"></i> May 27, 2015</span>
                                        </div>
                                        <div class="noto-message">
                                            Saved “<a href="blog-detail-2.html">Omni-onepage app template</a>” to Inspiration
                                            <a class="portfolio-link type-2 clearfix" href="blog-detail-2.html">
                                                <img src="{{ asset('img/p_link_30.jpg') }}" alt="">
                                                <img src="{{ asset('img/p_link_30.jpg') }}" alt="">
                                                <img src="{{ asset('img/p_link_30.jpg') }}" alt="">
                                                <img src="{{ asset('img/p_link_30.jpg') }}" alt="">
                                                <img src="{{ asset('img/p_link_30.jpg') }}" alt="">
                                                <div class="color_bg">
                                                    <span>view portfolio</span>
                                                    <span class="child"></span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="noto-entry">
                                <div class="noto-content clearfix">
                                    <div class="noto-img">
                                        <a href="blog-detail-2.html">
                                            <img src="{{ asset('img/c7.jpg') }}" alt="" class="be-ava-comment">
                                        </a>
                                    </div>
                                    <div class="noto-text">
                                        <div class="noto-text-top">
                                            <span class="noto-name"><a href="blog-detail-2.html">v-a studio</a></span>
                                            <span class="noto-date"><i class="fa fa-clock-o"></i> May 27, 2015</span>
                                        </div>
                                        <div class="noto-message">
                                            <a class="noto-left" class="" href="blog-detail-2.html">
                                                <img src="{{ asset('img/n_pop_1.jpg') }}" alt="">
                                            </a>
                                            Saved “<a href="blog-detail-2.html">Omni-onepage app template</a>” to Inspiration
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="noto-entry">
                                <div class="noto-content clearfix">
                                    <div class="noto-img">
                                        <a href="blog-detail-2.html">
                                            <img src="{{ asset('img/c8.jpg') }}" alt="" class="be-ava-comment">
                                        </a>
                                    </div>
                                    <div class="noto-text">
                                        <div class="noto-text-top">
                                            <span class="noto-name"><a href="blog-detail-2.html">Hoang Nguyen</a></span>
                                            <span class="noto-date"><i class="fa fa-clock-o"></i> May 27, 2015</span>
                                        </div>
                                        <div class="noto-message">
                                            <a class="noto-left" class="" href="blog-detail-2.html">
                                                <img src="{{ asset('img/n_pop_2.jpg') }}" alt="">
                                            </a>
                                            Awesome, love the big whitespace and also everything between :)
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="noto-entry">
                                <div class="noto-content clearfix">
                                    <div class="noto-img">
                                        <a href="blog-detail-2.html">
                                            <img src="{{ asset('img/c9.jpg') }}" alt="" class="be-ava-comment">
                                        </a>
                                    </div>
                                    <div class="noto-text">
                                        <div class="noto-text-top">
                                            <span class="noto-name"><a href="blog-detail-2.html">Cüneyt ŞEN</a></span>
                                            <span class="noto-date"><i class="fa fa-clock-o"></i> May 27, 2015</span>
                                        </div>
                                        <a href="blog-detail-2.html" class="noto-message">
                                            Start following your work
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--<a class="messages-popup" href="blog-detail-2.html">--}}
                        {{--<i class="fa fa-envelope-o"></i>--}}
                        {{--<span class="noto-count">4</span>--}}
                    {{--</a>--}}
                    <div class="noto-popup messages-block">
                        <div class="noto-label">Your Messages <span class="noto-label-links"><a href="messages-2.html">compose</a><a href="messages.html">View all messages</a></span></div>
                        <div class="noto-body">
                            <div class="noto-entry style-2">
                                <div class="noto-content clearfix">
                                    <div class="noto-img">
                                        <a href="blog-detail-2.html">
                                            <img src="{{ asset('img/c1.png') }}" alt="" class="be-ava-comment">
                                        </a>
                                    </div>
                                    <div class="noto-text">
                                        <div class="noto-text-top">
                                            <span class="noto-name"><a href="blog-detail-2.html">Ravi Sah</a></span>
                                            <span class="noto-date"><i class="fa fa-clock-o"></i> May 27, 2015</span>
                                        </div>
                                        <div class="noto-message">Sed velit mauris, pulvinar sit amet accumsan vitae, egestas, pulvinar sit amet accumsan vitae, egestas</div>
                                    </div>
                                </div>
                            </div>
                            <div class="noto-entry style-2">
                                <div class="noto-content clearfix">
                                    <div class="noto-img">
                                        <a href="blog-detail-2.html">
                                            <img src="{{ asset('img/c6.jpg') }}" alt="" class="be-ava-comment">
                                        </a>
                                    </div>
                                    <div class="noto-text">
                                        <div class="noto-text-top">
                                            <span class="noto-name"><a href="blog-detail-2.html">Louis Paquet</a></span>
                                            <span class="noto-date"><i class="fa fa-clock-o"></i> May 27, 2015</span>
                                        </div>
                                        <div class="noto-message">
                                            Pellentesque habitant morbi tristique senectus et netus tristique senectus
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="noto-entry style-2">
                                <div class="noto-content clearfix">
                                    <div class="noto-img">
                                        <a href="blog-detail-2.html">
                                            <img src="{{ asset('img/c9.jpg') }}" alt="" class="be-ava-comment">
                                        </a>
                                    </div>
                                    <div class="noto-text">
                                        <div class="noto-text-top">
                                            <span class="noto-name"><a href="blog-detail-2.html">Cüneyt ŞEN</a></span>
                                            <span class="noto-date"><i class="fa fa-clock-o"></i> May 27, 2015</span>
                                        </div>
                                        <div class="noto-message">
                                            Sed id erat vitae libero malesuada dictum vel sit amet eros
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="noto-entry style-2">
                                <div class="noto-content clearfix">
                                    <div class="noto-img">
                                        <a href="blog-detail-2.html">
                                            <img src="{{ asset('img/c10.jpg') }}" alt="" class="be-ava-comment">
                                        </a>
                                    </div>
                                    <div class="noto-text">
                                        <div class="noto-text-top">
                                            <span class="noto-name"><a href="blog-detail-2.html">Tomasz Mazurczak</a></span>
                                            <span class="noto-date"><i class="fa fa-clock-o"></i> May 27, 2015</span>
                                        </div>
                                        <div class="noto-message">
                                            In molestie libero quis cursus ullamcorper eu rhoncus magna
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--<div class="be-drop-down color-4 size-2">--}}
                        {{--<span class="be-dropdown-content">ADD WORK</span>--}}
                        {{--<ul class="drop-down-list">--}}
                            {{--<li><a>Featured</a></li>--}}
                            {{--<li><a>Most Appreciated</a></li>--}}
                            {{--<li><a>Most Viewed</a></li>--}}
                            {{--<li><a>Most Discussed</a></li>--}}
                            {{--<li><a>Most Recent</a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                    <div class="be-drop-down login-user-down">
                        <img class="login-user" src="@if(!empty(auth()->user()->avatar)){{ asset('img/users/' . auth()->user()->getPathFromHash(auth()->user()->avatar)) }}@endif" alt="">
                        <a class="mr-4" href="{{ route('my_profile') }}">
                            <span class="be-dropdown-content">{{ auth()->user()->name . ' ' . auth()->user()->surname }}</span>
                        </a>
                        <ul class="drop-down-list">
                            <li><a id="my-profile-link" href="{{ route('my_profile') }}"
                                onclick="event.preventDefault();
                                document.getElementById('my-profile-link').click();"
                                >Мой профиль</a></li>
                            <li><a href="{{ route('my_profile') }}">Мои резюме</a></li>
                            <li><a href="{{ route('my_profile') }}">Settings</a></li>
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                    {{ __('Выйти') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                            {{ __('Выйти') }}
                        </a>
                    </div>
                </div>
            </div>
            @else
                {{--<div class="login-header-block header-buttons">--}}
                    {{--<div class="login_block">--}}
                        {{--<a class="btn-login btn color-1 size-2 hover-2" href="blog-detail-2.html"><i class="fa fa-user"></i>Войти</a>--}}
                        {{--<a class="be-register btn color-1 size-2 hover-2"><i class="fa fa-lock"></i>Регистрация</a>--}}
                        {{--<a class="btn color-1 size-2 hover-2" href="{{ route('login') }}"><i class="fa fa-user"></i>Войти</a>--}}
                        {{--<a class="btn color-1 size-2 hover-2" href="{{ route('register') }}"><i class="fa fa-lock"></i>Регистрация</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            @endauth

            <div class="header-menu-block">
                <button class="cmn-toggle-switch cmn-toggle-switch__htx"><span></span></button>
                <ul class="header-menu" id="one">
                    @auth
                    <li @if(Request::route()->getName() === 'my_profile') class="active-header" @endif><a href="{{ route('my_profile') }}">Моя страница</a></li>
                    @endauth
                    <li><a href="{{ route('interviews.index') }}">Интервью</a></li>
                    <li @if(strpos(Request::route()->getName(), 'my_resume')) class="active-header" @endif><a href="{{ route('my_resume.index') }}">Резюме</a><i class="fa fa-angle-down"></i>
                        <ul>
                            <li><a href="{{ route('my_resume.index') }}">Мои резюме</a></li>
                            <li><a href="{{ route('my_resume.create') }}">Создать новое резюме</a></li>
                            {{--<li><a href="messages.html">Проекты</a></li>--}}
                            {{--<li><a href="messages-2.html">Мероприятия</a></li>--}}
                        </ul>
                    </li>
                    @auth
                        @if(auth()->user()->role->name === 'Administrator')
                        <li><a href="#">Администратор</a><i class="fa fa-angle-down"></i>
                            <ul>
                                <li><a href="{{ route('tags.index') }}">Теги</a></li>
                                <li><a href="{{ route('roles.index') }}">Роли</a></li>
                                <li><a href="{{ route('roles.index') }}">Права доступа</a></li>
                                <li><a href="{{ route('prof_categories.index') }}">Проф. категории</a></li>
                            </ul>
                        </li>
                        @endif
                        @if(auth()->user()->role->name === 'Administrator' or auth()->user()->role->name === 'Moderator')
                            <li><a href="#">Модератор</a><i class="fa fa-angle-down"></i>
                                <ul>
                                    <li><a href="{{ route('admin.interviews.index') }}">Интервью</a></li>
                                    <li><a href="{{ route('issues.admin.index') }}">Обращения</a></li>
                                </ul>
                            </li>
                        @endif
                            <li>
                                <a href="{{ route('issues.index') }}">Помощь</a><i class="fa fa-angle-down"></i>
                            </li>
                    @endauth
                    {{--<li><a href="#">Новости</a></li>--}}
                    {{--<li><a href="#">Поиск</a><i class="fa fa-angle-down"></i>--}}
                        {{--<ul>--}}
                            {{--<li><a href="author-edit.html">Резюме</a></li>--}}
                            {{--<li><a href="author-login.html">Проекты (поиск работы)</a></li>--}}
                            {{--<li><a href="messages.html">Проекты</a></li>--}}
                            {{--<li><a href="messages-2.html">Мероприятия</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li><a href="#">Помощь</a></li>--}}
                </ul>
            </div>
        </div>
    </div>
</header>